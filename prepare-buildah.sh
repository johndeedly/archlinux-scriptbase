#!/usr/bin/env bash

# initialize root
USER="$(id -un)"
if [ ! "$EUID" -eq 0 ]; then
  sudo "$0" "$@"
  exit $?
else
  echo "Running as user '$USER'"
fi

# parse parameters
CONTAINERNAME=""
POSITIONAL=()
while [ $# -gt 0 ]; do
  key="$1"
  case $key in
    -h|--help)
      shift
      echo "TODO"
      exit 1
      ;;
    -n|--containername)
      CONTAINERNAME="$2"
      shift
      shift
      echo "[#] using CONTAINERNAME=${CONTAINERNAME}"
      ;;
    *)
      POSITIONAL+=("$1") # save it in an array for later
      shift
      ;;
  esac
done
set -- "${POSITIONAL[@]}" # restore positional parameters

buildah --cap-add=SYS_CHROOT,NET_ADMIN,NET_RAW --name "${CONTAINERNAME}" from scratch
buildah config --entrypoint "/usr/sbin/init" --cmd '["--log-level=info", "--unit=multi-user.target"]' "${CONTAINERNAME}"
scratchmnt=$(buildah mount "${CONTAINERNAME}")
mount --bind "${scratchmnt}" /mnt
if ! mountpoint -q -- /mnt; then
  1>&2 echo "[!] /mnt doesn't appear to be mounted, aborting to prevent accidentally filling up the main filesystem"
  buildah umount "${CONTAINERNAME}"
  buildah rm "${CONTAINERNAME}"
  exit -1
fi

pacstrap -K -c /mnt base

systemd-nspawn --capability=CAP_SYS_CHROOT,CAP_NET_ADMIN,CAP_NET_RAW --pipe -D /mnt <<EOX
# prepare systemd for container usage
# - disable login
ln -s /dev/null /etc/systemd/system/console-getty.service

# - disable first boot interactive config
ln -s /dev/null /etc/systemd/system/systemd-firstboot.service

# - disable time sync agent
ln -s /dev/null /etc/systemd/system/ntpd.service

# - logging to podman
mkdir -p /var/log/journal
sed -i 's/^#ForwardToConsole.*/ForwardToConsole=yes/' /etc/systemd/journald.conf

# - machine id setup for pam (systemd)
systemd-machine-id-setup

# - allow root login from /dev/pts/*
tee -a /etc/securetty <<EOS
pts/0
pts/1
pts/2
pts/3
pts/4
pts/5
pts/6
pts/7
pts/8
pts/9
EOS

# - protect /mnt from being writable to root user (used only as a mount point, protection against filling up your '/' on mount errors)
chmod a-rwx /mnt
chattr +i /mnt
EOX
