#!/usr/bin/env bash

# configure error handling
set -e -o pipefail

# read administrative database to get usernames, group names and home directories
ROOTID=$(getent passwd 0 | cut -d: -f1)
ROOTGRP=$(getent group "$(getent passwd 0 | cut -d: -f4)" | cut -d: -f1)
ROOTHOME=$(getent passwd 0 | cut -d: -f6)
USERID=$(getent passwd 1000 | cut -d: -f1)
USERGRP=$(getent group "$(getent passwd 1000 | cut -d: -f4)" | cut -d: -f1)
USERHOME=$(getent passwd 1000 | cut -d: -f6)
TEMPID=$(getent passwd 4815 | cut -d: -f1)
TEMPGRP=$(getent group "$(getent passwd 4815 | cut -d: -f4)" | cut -d: -f1)
TEMPHOME=$(getent passwd 4815 | cut -d: -f6)
echo "admin: ${ROOTID}|${ROOTGRP}|${ROOTHOME}"
echo "user: ${USERID}|${USERGRP}|${USERHOME}"
echo "temp: ${TEMPID}|${TEMPGRP}|${TEMPHOME}"

# install packages
pacman -S --needed --noconfirm syslinux darkhttpd nfs-utils nbd samba iptraf-ng ntp step-ca step-cli dnsmasq ntp

DHCP_ADDITIONAL_SETUP=(
  "dhcp-option=option:dns-server,192.168.123.128\n"
  "dhcp-option=option6:dns-server,[2001:db8:7b:1::]\n"
  "dhcp-option=option:ntp-server,192.168.123.128\n"
  "dhcp-option=option6:ntp-server,[2001:db8:7b:1::]\n"
  "\n"
  "# Override the default route supplied by dnsmasq, which assumes the"
)

PXESETUP=(
  "dhcp-match=set:efi-x86_64,option:client-arch,7\n"
  "dhcp-match=set:efi-x86_64,option:client-arch,9\n"
  "dhcp-match=set:efi-x86,option:client-arch,6\n"
  "dhcp-match=set:bios,option:client-arch,0\n"

  "dhcp-boot=tag:efi-x86_64,efi64\/syslinux.efi\n"
  "dhcp-boot=tag:efi-x86,efi32\/syslinux.efi\n"
  "dhcp-boot=tag:bios,bios\/lpxelinux.0"
)

DHCP_209_SETUP=(
  "dhcp-option-force=tag:efi-x86_64,209,pxelinux.cfg\/default\n"
  "dhcp-option-force=tag:efi-x86,209,pxelinux.cfg\/default\n"
  "dhcp-option-force=tag:bios,209,pxelinux.cfg\/default"
)

DHCP_210_SETUP=(
  "dhcp-option-force=tag:efi-x86_64,210,efi64\/\n"
  "dhcp-option-force=tag:efi-x86,210,efi32\/\n"
  "dhcp-option-force=tag:bios,210,bios\/"
)

# configure network
cp /root/router/20-all.link /etc/systemd/network/
cp /root/router/20-extern.network /etc/systemd/network/
cp /root/router/20-intern.network /etc/systemd/network/
rm /etc/systemd/network/20-wired.network
# disable dns
sed -i '0,/^#\?port.*/s//port=0/' /etc/dnsmasq.conf
tee /etc/default/dnsmasq <<EOF
DNSMASQ_OPTS="-p0"
EOF
# enable dns server over systemd-resolved
sed -i '0,/^#\?DNSStubListener.*/s//DNSStubListener=yes\nDNSStubListenerExtra=0.0.0.0\nDNSStubListenerExtra=::/' /etc/systemd/resolved.conf

sed -i '0,/^#\?domain-needed.*/s//domain-needed/' /etc/dnsmasq.conf
sed -i '0,/^#\?bogus-priv.*/s//bogus-priv/' /etc/dnsmasq.conf
sed -i '0,/^#\?local=.*/s//local=\/locally\//' /etc/dnsmasq.conf
sed -i '0,/^#\?domain=.*/s//domain=locally/' /etc/dnsmasq.conf
sed -i '0,/^#\?dhcp-range=.*/s//dhcp-range=192.168.123.1,192.168.123.127,255.255.255.0,12h/' /etc/dnsmasq.conf
sed -i '0,/^#\?dhcp-range=.*::.*/s//dhcp-range=2001:db8:7b::1,2001:db8:7b::ffff,64,12h/' /etc/dnsmasq.conf
sed -i '0,/^# Override the default route.*/s//'"${DHCP_ADDITIONAL_SETUP[*]}"'/' /etc/dnsmasq.conf
sed -i '0,/^#\?enable-ra.*/s//enable-ra/' /etc/dnsmasq.conf
sed -i '0,/^#\?enable-tftp.*/s//enable-tftp/' /etc/dnsmasq.conf
sed -i '0,/^#\?tftp-root=.*/s//tftp-root=\/srv\/tftp/' /etc/dnsmasq.conf
sed -i '0,/^#\?log-dhcp.*/s//log-dhcp/' /etc/dnsmasq.conf
sed -i '0,/^#\?log-queries.*/s//log-queries/' /etc/dnsmasq.conf
sed -i '0,/^#\?dhcp-boot=.*/s//'"${PXESETUP[*]}"'/' /etc/dnsmasq.conf
sed -i '0,/^#\?dhcp-option-force=209.*/s//'"${DHCP_209_SETUP[*]}"'/' /etc/dnsmasq.conf
sed -i '0,/^#\?dhcp-option-force=210.*/s//'"${DHCP_210_SETUP[*]}"'/' /etc/dnsmasq.conf

# configure wireguard server on port 51820
cp /root/router/wg.conf /etc/sysctl.d/
cp /etc/ufw/before.rules /etc/ufw/before.rules.bak
cat /etc/ufw/before.rules.bak /root/router/before.rules.append > /etc/ufw/before.rules

cp /root/router/wgup.sh /root/wgup.sh
chmod +x /root/wgup.sh

cp /root/router/wireguard.service /etc/systemd/system/
systemctl enable wireguard.service

# configure tftp
mkdir -p /srv/tftp/{bios,efi32,efi64}/pxelinux.cfg
cp -ar /usr/lib/syslinux/bios /srv/tftp/
cp -ar /usr/lib/syslinux/efi32 /srv/tftp/
cp -ar /usr/lib/syslinux/efi64 /srv/tftp/
cp /root/router/pxelinux.default /srv/tftp/bios/pxelinux.cfg/default
cp /root/router/pxelinux.default /srv/tftp/efi32/pxelinux.cfg/default
cp /root/router/pxelinux.default /srv/tftp/efi64/pxelinux.cfg/default
mkdir -p /srv/tftp/{,bios,efi32,efi64}/arch/x86_64

# configure http
mkdir -p /srv/http/arch/x86_64
cp /root/router/darkhttpd.service /etc/systemd/system/
systemctl enable darkhttpd.service

# configure nfs
mkdir -p /srv/nfs/arch/x86_64
sed -i '0,/^\[mountd\].*/s//[mountd]\nport=20048/' /etc/nfs.conf
cp /root/router/exports /etc/exports

# configure nbd
mkdir -p /srv/nbd/arch/x86_64
cp /root/router/nbd.config /etc/nbd-server/config
cp /root/router/nbd.allow /etc/nbd-server/allow

# configure cifs
mkdir -p /srv/cifs/arch/x86_64
cp /root/router/smb.conf /etc/samba/smb.conf

# change default access rights
chown -R root:root /srv/tftp /srv/http /srv/nfs /srv/nbd /srv/cifs
find /srv/tftp /srv/http /srv/nfs /srv/nbd /srv/cifs -type d -exec chmod 755 {} \;
find /srv/tftp /srv/http /srv/nfs /srv/nbd /srv/cifs -type f -exec chmod 644 {} \;

# the router is it's own acme protocol certificate authority
useradd step
mkdir -p /home/step /var/log/step-ca
chown -R step:step /home/step /var/log/step-ca
cp /root/router/step-ca.service /etc/systemd/system/

mkdir -p /home/step/.step
chown -R step:step /home/step/.step
openssl rand -base64 36 | tee /home/step/.step/pwd
chown step:step /home/step/.step/pwd
chmod 400 /home/step/.step/pwd

su -s /bin/bash - step <<EOS
step-cli ca init --deployment-type=standalone --name=locally --dns=192.168.123.128 --dns=192.168.144.1 --dns=2001:db8:7b:1:: --dns=router.locally --address=:8443 --provisioner=step-ca@router.locally --password-file=/home/step/.step/pwd --acme
sed -i '0,/"name": "acme".*/s//"name": "acme",\n\t\t\t\t"claims": {\n\t\t\t\t\t"maxTLSCertDuration": "2160h",\n\t\t\t\t\t"defaultTLSCertDuration": "2160h"\n\t\t\t\t}/' /home/step/.step/config/ca.json
EOS

# configure ntp
cp /root/router/ntp.conf /etc/ntp.conf

# enable all configured services
systemctl enable dnsmasq nfs-server nbd smb step-ca ntpd

# configure firewall
ufw disable

# remove existing ssh rule
ufw delete limit ssh

# ==========
# eth0 - extern
# ==========
ufw allow in on eth0 proto tcp to any port 51820 comment 'wireguard tcp on extern'
ufw allow in on eth0 proto udp to any port 51820 comment 'wireguard udp on extern'

# ==========
# eth1 - intern
# ==========
ufw allow in on eth1 to any port bootps comment 'bootps on intern'
ufw allow in on eth1 to any port ssh comment 'ssh on intern'
ufw allow in on eth1 to any port 53 comment 'dns on intern'
ufw allow in on eth1 to any port tftp comment 'tftp on intern'
ufw allow in on eth1 to any port 80 comment 'http on intern'
ufw allow in on eth1 to any port ntp comment 'ntp on intern'
ufw allow in on eth1 to any port 111 comment 'nfs on intern'
ufw allow in on eth1 to any port 2049 comment 'nfs on intern'
ufw allow in on eth1 to any port 20048 comment 'nfs on intern'
ufw allow in on eth1 to any port nbd comment 'nbd on intern'
ufw allow in on eth1 to any port 445 comment 'cifs on intern'
ufw allow in on eth1 to any port 139 comment 'cifs on intern'
ufw allow in on eth1 to any port 8443 comment 'step-ca on intern'
ufw allow in on eth1 to any port 51820 comment 'wireguard on intern'

ufw route deny in on eth1 out on eth0 to any port 53 comment 'block dns from intern to extern'
ufw route deny in on eth1 out on eth0 to any port 853 comment 'block dns from intern to extern'
ufw route deny in on eth1 out on eth0 to any port 5353 comment 'block dns from intern to extern'
ufw route deny in on wg0 out on eth0 to any port 53 comment 'block dns from wireguard to extern'
ufw route deny in on wg0 out on eth0 to any port 853 comment 'block dns from wireguard to extern'
ufw route deny in on wg0 out on eth0 to any port 5353 comment 'block dns from wireguard to extern'
ufw route allow in on eth1 out on eth0 comment 'allow forward from intern to extern'
ufw route allow in on eth1 out on wg0 comment 'allow forward from intern to wireguard'
ufw route allow in on eth1 out on eth1 comment 'allow local intern forwarding'

# ==========
# wg0 - wireguard
# ==========
ufw route allow in on wg0 out on eth0 comment 'allow forward from wireguard to extern'
ufw route allow in on wg0 out on eth1 comment 'allow forward from wireguard to intern'
ufw route allow in on wg0 out on wg0 comment 'allow local wireguard forwarding'

# ==========
# lo - loopback
# ==========
ufw allow in on lo comment 'allow loopback in'
ufw route allow in on lo out on lo comment 'allow loopback forward'

ufw enable

# == cleanup everything ==
# - tmpusr installation data
if [ -d "${TEMPHOME}" ]; then
  /usr/bin/rm -r "${TEMPHOME}"/* || true
fi
# - package cache
echo -en "y\ny\n" | ( LC_ALL=C pacman -Scc )
# - logs
if [ -f /run/systemd/journal/io.systemd.journal ]; then
  journalctl --rotate
  journalctl --vacuum-time=1s
fi
fstrim --all
# - do not rebuild cache on boot
tee /etc/.updated /var/.updated <<EOF
# For details, see manpage of systemd.unit -> 'ConditionNeedsUpdate'.
TIMESTAMP_NSEC=$(date +%s%N)
EOF
chmod 644 /etc/.updated /var/.updated
