#!/usr/bin/env bash

# initialize root
USER="$(id -un)"
if [ ! "$EUID" -eq 0 ]; then
  sudo "$0" "$@"
  exit $?
else
  echo "Running as user '$USER'"
fi

# parse parameters
CONTAINERNAME=""
POSITIONAL=()
while [ $# -gt 0 ]; do
  key="$1"
  case $key in
    -h|--help)
      shift
      echo "TODO"
      exit 1
      ;;
    -n|--containername)
      CONTAINERNAME="$2"
      shift
      shift
      echo "[#] using CONTAINERNAME=${CONTAINERNAME}"
      ;;
    *)
      POSITIONAL+=("$1") # save it in an array for later
      shift
      ;;
  esac
done
set -- "${POSITIONAL[@]}" # restore positional parameters

fuser -km /mnt || true
sync
umount /mnt || true
buildah umount "${CONTAINERNAME}"

buildah commit "${CONTAINERNAME}" "${CONTAINERNAME}"

if [ -f "${CONTAINERNAME}.tar" ]; then
  /usr/bin/rm "${CONTAINERNAME}.tar"
fi

podman save -o "${CONTAINERNAME}.tar" "${CONTAINERNAME}"
buildah rm "${CONTAINERNAME}"
podman image rm "${CONTAINERNAME}"
podman image prune -f
