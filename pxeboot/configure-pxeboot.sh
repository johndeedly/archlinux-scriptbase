#!/usr/bin/env bash

# configure error handling
set -e -o pipefail

# read administrative database to get usernames, group names and home directories
ROOTID=$(getent passwd 0 | cut -d: -f1)
ROOTGRP=$(getent group "$(getent passwd 0 | cut -d: -f4)" | cut -d: -f1)
ROOTHOME=$(getent passwd 0 | cut -d: -f6)
USERID=$(getent passwd 1000 | cut -d: -f1)
USERGRP=$(getent group "$(getent passwd 1000 | cut -d: -f4)" | cut -d: -f1)
USERHOME=$(getent passwd 1000 | cut -d: -f6)
TEMPID=$(getent passwd 4815 | cut -d: -f1)
TEMPGRP=$(getent group "$(getent passwd 4815 | cut -d: -f4)" | cut -d: -f1)
TEMPHOME=$(getent passwd 4815 | cut -d: -f6)
echo "admin: ${ROOTID}|${ROOTGRP}|${ROOTHOME}"
echo "user: ${USERID}|${USERGRP}|${USERHOME}"
echo "temp: ${TEMPID}|${TEMPGRP}|${TEMPHOME}"

# install packages
pacman -S --needed --noconfirm mkinitcpio-nfs-utils curl ca-certificates-utils cifs-utils nfs-utils nbd amd-ucode intel-ucode

mkdir -p /srv/{tftp,http,nfs,nbd,cifs}/arch/x86_64

# create skeleton for pxe boot mkinitcpio
mkdir -p /usr/lib/initcpio/{install,hooks}
cp /root/pxeboot/install/* /usr/lib/initcpio/install/
cp /root/pxeboot/hooks/* /usr/lib/initcpio/hooks/
mkdir -p /etc/mkinitcpio{,.conf}.d
cp /root/pxeboot/pxe.conf /etc/mkinitcpio.conf.d/
cp /root/pxeboot/pxe.preset /etc/mkinitcpio.d/

# generate initcpio for pxe boot
mkdir -p /root/mkinitcpio_tmp
mkinitcpio -p pxe -t /root/mkinitcpio_tmp

# disable pxe preset and config
mv /etc/mkinitcpio.conf.d/pxe.conf /etc/mkinitcpio.conf.d/pxe.conf.disabled
mv /etc/mkinitcpio.d/pxe.preset /etc/mkinitcpio.d/pxe.preset.disabled

# copy everything to the tftp folders
cp /boot/vmlinuz-linux /boot/initramfs-linux-pxe.img /srv/tftp/arch/x86_64/
cp /boot/intel-ucode.img /boot/amd-ucode.img /srv/tftp/arch/

ITER=( "bios" "efi32" "efi64" )
for item in "${ITER[@]}"
do
  mkdir -p "/srv/tftp/$item/arch/x86_64"
  ln /srv/tftp/arch/x86_64/vmlinuz-linux "/srv/tftp/$item/arch/x86_64/vmlinuz-linux"
  ln /srv/tftp/arch/x86_64/initramfs-linux-pxe.img "/srv/tftp/$item/arch/x86_64/initramfs-linux-pxe.img"
  ln /srv/tftp/arch/intel-ucode.img "/srv/tftp/$item/arch/intel-ucode.img"
  ln /srv/tftp/arch/amd-ucode.img "/srv/tftp/$item/arch/amd-ucode.img"
done
unset item
unset ITER

rm -rf /root/mkinitcpio_tmp

# fix file access
chown -R root:root /srv/tftp /srv/http /srv/nfs /srv/nbd /srv/cifs
find /srv/tftp /srv/http /srv/nfs /srv/nbd /srv/cifs -type d -exec chmod 755 {} \;
find /srv/tftp /srv/http /srv/nfs /srv/nbd /srv/cifs -type f -exec chmod 644 {} \;

# == cleanup everything ==
# - tmpusr installation data
if [ -d "${TEMPHOME}" ]; then
  /usr/bin/rm -r "${TEMPHOME}"/* || true
fi
# - package cache
echo -en "y\ny\n" | ( LC_ALL=C pacman -Scc )
# - logs
if [ -f /run/systemd/journal/io.systemd.journal ]; then
  journalctl --rotate
  journalctl --vacuum-time=1s
fi
fstrim --all
# - do not rebuild cache on boot
tee /etc/.updated /var/.updated <<EOF
# For details, see manpage of systemd.unit -> 'ConditionNeedsUpdate'.
TIMESTAMP_NSEC=$(date +%s%N)
EOF
chmod 644 /etc/.updated /var/.updated
