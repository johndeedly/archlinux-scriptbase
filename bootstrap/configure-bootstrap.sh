#!/usr/bin/env bash

# configure error handling
set -e -o pipefail

# predefine functions
function installdeps() {
  local TMPPKG=$(mktemp)
  echo "install dependencies for $@"
  while (( "$#" )); do
    curl -sL "https://aur.archlinux.org/cgit/aur.git/plain/PKGBUILD?h=$1" > "${TMPPKG}"
    grep -e 'depends' "${TMPPKG}" && (
      unset depends
      unset makedepends
      unset checkdepends
      source "${TMPPKG}"
      ALLDEPENDS=("${depends[@]}" "${makedepends[@]}" "${checkdepends[@]}")
      for pkg in "${ALLDEPENDS[@]}"; do
        if [ ! -z "${pkg}" ]; then
          pacman -S --needed --noconfirm --asdeps --color=auto "${pkg}" || true
        fi
      done
    ) || echo 1>&2 "no dependencies parsed for $1"
    shift
  done
  /usr/bin/rm "${TMPPKG}"
}

# enable default repos
curl -sL 'https://archlinux.org/mirrorlist/?country=AT&country=BE&country=DK&country=FI&country=FR&country=DE&country=IT&country=NL&country=NO&country=PL&country=ES&country=SE&country=CH&country=GB&protocol=http&protocol=https&ip_version=4&ip_version=6&use_mirror_status=on' | \
    sed -e 's/#Server/Server/g' | \
    tee /etc/pacman.d/mirrorlist

# enable multilib and parallel downloads
if grep -q "\[multilib\]"; then
    sed -i '/^#\?\[multilib\]$/{N;s/^#\?\[multilib\]\n#\?Include.*/[multilib]\nInclude = \/etc\/pacman.d\/mirrorlist/;}' /etc/pacman.conf
else
    tee -a /etc/pacman.conf <<EOS

[multilib]
Include = /etc/pacman.d/mirrorlist
EOS
fi
sed -i 's/^#\?ParallelDownloads.*/ParallelDownloads = 5/' /etc/pacman.conf
sed -i 's/^#\?NoExtract.*/NoExtract = usr\/share\/help\/* !usr\/share\/help\/C\/* !usr\/share\/help\/de*\/* !usr\/share\/help\/en*\/*\nNoExtract = usr\/share\/man\/* !usr\/share\/man\/de*\/* !usr\/share\/man\/man*\/*\nNoExtract = usr\/share\/backgrounds\/*\/* !usr\/share\/backgrounds\/archlinux\/*/' /etc/pacman.conf
pacman -Sy --color=auto

# prepare opendoas as drop in replacement of sudo
pacman -S --noconfirm --color=auto opendoas fakeroot binutils
ln -s /usr/bin/doas /usr/bin/sudo
cp /root/bootstrap/doas.conf /etc/doas.conf
chmod 0400 /etc/doas.conf
sed -i 's/^#IgnorePkg.*/IgnorePkg   = sudo/' /etc/pacman.conf

# i18n
sed -i 's/^#\?de_DE.UTF-8 UTF-8/de_DE.UTF-8 UTF-8/' /etc/locale.gen
locale-gen
echo "LANG=de_DE.UTF-8" > /etc/locale.conf
rm /etc/localtime || true
ln -s /usr/share/zoneinfo/CET /etc/localtime
loadkeys de || true
tee /etc/vconsole.conf <<EOF
KEYMAP=de
XKBLAYOUT=de
XKBMODEL=pc105
EOF

# enable passwords and one user
echo -e "$( cat /root/bootstrap/root.pwd )\n$( cat /root/bootstrap/root.pwd )" | (passwd root)
useradd -m -u 1000 -g users -G wheel "$(</root/bootstrap/user.usr)"
echo -e "$( cat /root/bootstrap/user.pwd )\n$( cat /root/bootstrap/user.pwd )" | (passwd "$(</root/bootstrap/user.usr)")

# create tmpusr for building sudo-dummy and vim-dummy
groupadd -g 4815 tmpusr
useradd -m -u 4815 -g tmpusr -s /bin/false tmpusr

# read administrative database to get usernames, group names and home directories
ROOTID=$(getent passwd 0 | cut -d: -f1)
ROOTGRP=$(getent group "$(getent passwd 0 | cut -d: -f4)" | cut -d: -f1)
ROOTHOME=$(getent passwd 0 | cut -d: -f6)
USERID=$(getent passwd 1000 | cut -d: -f1)
USERGRP=$(getent group "$(getent passwd 1000 | cut -d: -f4)" | cut -d: -f1)
USERHOME=$(getent passwd 1000 | cut -d: -f6)
TEMPID=$(getent passwd 4815 | cut -d: -f1)
TEMPGRP=$(getent group "$(getent passwd 4815 | cut -d: -f4)" | cut -d: -f1)
TEMPHOME=$(getent passwd 4815 | cut -d: -f6)
echo "admin: ${ROOTID}|${ROOTGRP}|${ROOTHOME}"
echo "user: ${USERID}|${USERGRP}|${USERHOME}"
echo "temp: ${TEMPID}|${TEMPGRP}|${TEMPHOME}"

# create subuids and subgids
tee /etc/subuid <<EOF
${ROOTID}:100000:65536
${USERID}:165536:65536
${TEMPID}:231072:65536
EOF
tee /etc/subgid <<EOF
${ROOTID}:100000:65536
${USERID}:165536:65536
${TEMPID}:231072:65536
EOF

# create and install sudo-dummy
cp /root/bootstrap/sudo.pkgbuild "${TEMPHOME}"/PKGBUILD
chown "${TEMPID}:${TEMPGRP}" "${TEMPHOME}"/PKGBUILD
su -s /bin/bash - "${TEMPID}" <<EOS
makepkg --clean
EOS
pacman -U --needed --noconfirm --color=auto "${TEMPHOME}"/sudo-dummy-1-1-any.pkg.tar.zst

# install base-devel without sudo
pacman -S --noconfirm --color=auto base-devel

# create and install vim-dummy
cp /root/bootstrap/vim.pkgbuild "${TEMPHOME}"/PKGBUILD
chown "${TEMPID}:${TEMPGRP}" "${TEMPHOME}"/PKGBUILD
su -s /bin/bash - "${TEMPID}" <<EOS
makepkg --clean
EOS
pacman -U --needed --noconfirm --color=auto "${TEMPHOME}"/vim-dummy-1-1-any.pkg.tar.zst
ln -s /usr/bin/nvim /usr/bin/vim
ln -s /usr/bin/nvim /usr/bin/vi

# install standard packages
pacman -S --noconfirm --color=auto nano neovim htop btop dialog git lazygit ufw \
  bash-completion pacman-contrib openssh pv lshw libxml2 jq ly polkit \
  dotnet-host dotnet-sdk dotnet-runtime aspnet-runtime \
  dotnet-sdk-6.0 dotnet-runtime-6.0 aspnet-runtime-6.0 \
  python python-pip words python-setuptools python-wheel \
  wireguard-tools wget nfs-utils ncdu viu core/man man-pages-de trash-cli \
  gvfs gvfs-smb sshfs cifs-utils \
  unzip p7zip rsync mc lf fzf xdg-user-dirs xdg-utils \
  starship ttf-nerd-fonts-symbols

systemctl enable systemd-networkd systemd-resolved systemd-homed sshd ufw ly

# disable tty login
systemctl disable console-getty.service getty@tty2.service

# configure ly to have username and shell prefilled on first boot
tee /etc/ly/save <<EOF
${USERID}
0
EOF

su -s /bin/bash - "${USERID}" <<EOS
python -m pip install --user --break-system-packages --no-warn-script-location xkcdpass || true
EOS

# create skeleton for bash profile
cp /root/bootstrap/bash_profile /etc/skel/.bash_profile
cp /root/bootstrap/bash_profile "${USERHOME}"/.bash_profile
chown "${USERID}:${USERGRP}" "${USERHOME}"/.bash_profile
cp /root/bootstrap/bash_profile "${ROOTHOME}"/.bash_profile

# create skeleton for bashrc
cp /root/bootstrap/bashrc /etc/skel/.bashrc
cp /root/bootstrap/bashrc "${USERHOME}"/.bashrc
chown "${USERID}:${USERGRP}" "${USERHOME}"/.bashrc
cp /root/bootstrap/bashrc "${ROOTHOME}"/.bashrc

# create skeleton for starship
mkdir -p /etc/skel/.config "${ROOTHOME}"/.config "${USERHOME}"/.config
chown "${USERID}:${USERGRP}" "${USERHOME}"/.config
cp /root/bootstrap/starship.toml /etc/skel/.config/starship.toml
cp /root/bootstrap/starship.toml "${USERHOME}"/.config/starship.toml
chown "${USERID}:${USERGRP}" "${USERHOME}"/.config/starship.toml
cp /root/bootstrap/starship.toml "${ROOTHOME}"/.config/starship.toml

# create skeleton for powershell
mkdir -p /etc/skel/.config/powershell "${ROOTHOME}"/.config/powershell "${USERHOME}"/.config/powershell
chown "${USERID}:${USERGRP}" "${USERHOME}"/.config/powershell
cp /root/bootstrap/profile.ps1 /etc/skel/.config/powershell/Microsoft.PowerShell_profile.ps1
cp /root/bootstrap/profile.ps1 "${USERHOME}"/.config/powershell/Microsoft.PowerShell_profile.ps1
chown "${USERID}:${USERGRP}" "${USERHOME}"/.config/powershell/Microsoft.PowerShell_profile.ps1
cp /root/bootstrap/profile.ps1 "${ROOTHOME}"/.config/powershell/Microsoft.PowerShell_profile.ps1

# create skeleton for htop
mkdir -p /etc/skel/.config/htop "${ROOTHOME}"/.config/htop "${USERHOME}"/.config/htop
chown "${USERID}:${USERGRP}" "${USERHOME}"/.config/htop
cp /root/bootstrap/htoprc /etc/skel/.config/htop/htoprc
cp /root/bootstrap/htoprc "${USERHOME}"/.config/htop/htoprc
chown "${USERID}:${USERGRP}" "${USERHOME}"/.config/htop/htoprc
cp /root/bootstrap/htoprc "${ROOTHOME}"/.config/htop/htoprc

# create skeleton for nanorc
cp /root/bootstrap/nanorc /etc/skel/.nanorc
cp /root/bootstrap/nanorc "${USERHOME}"/.nanorc
chown "${USERID}:${USERGRP}" "${USERHOME}"/.nanorc
cp /root/bootstrap/nanorc "${ROOTHOME}"/.nanorc

# create skeleton for inputrc
cp /root/bootstrap/inputrc /etc/skel/.inputrc
cp /root/bootstrap/inputrc "${USERHOME}"/.inputrc
chown "${USERID}:${USERGRP}" "${USERHOME}"/.inputrc
cp /root/bootstrap/inputrc "${ROOTHOME}"/.inputrc

# configure script to execute on first user login after boot
cp /root/bootstrap/userlogin.service /etc/systemd/user/userlogin.service
cp /root/bootstrap/userlogin.sh /usr/bin/userlogin.sh
chmod +x /usr/bin/userlogin.sh
systemctl --global enable userlogin.service

# install NvChad environment
su -s /bin/bash - "${USERID}" <<EOS
mkdir -p "${USERHOME}/.config" "${USERHOME}/.local/share"
git clone 'https://github.com/NvChad/NvChad' "${USERHOME}/.config/nvim" --depth 1
EOS
cp -r /root/bootstrap/nvim-lua-custom "${USERHOME}/.config/nvim/lua/custom"
chown -R "${USERID}:${USERGRP}" "${USERHOME}/.config/nvim/lua/custom"
su -s /bin/bash - "${USERID}" <<EOS
nvim -es -u "${USERHOME}/.config/nvim/init.lua" -c ":MasonInstallAll" -c ":TSInstall all" -c ":Lazy sync | Lazy load all" -c ":qall!" || true
EOS
mkdir -p /etc/skel/.config/nvim /etc/skel/.local/share/nvim \
  "${ROOTHOME}/.config/nvim" "${ROOTHOME}/.local/share/nvim"
cp -r "${USERHOME}/.config/nvim/"* /etc/skel/.config/nvim/
cp -r "${USERHOME}/.local/share/nvim/"* /etc/skel/.local/share/nvim/
cp -r "${USERHOME}/.config/nvim/"* "${ROOTHOME}/.config/nvim/"
cp -r "${USERHOME}/.local/share/nvim/"* "${ROOTHOME}/.local/share/nvim"

# enable ntfs kernel support (since 5.15)
echo "ntfs3" | tee /etc/modules-load.d/ntfs3.conf
tee /etc/udev/rules.d/50-ntfs.rules <<EOF
SUBSYSTEM=="block", ENV{ID_FS_TYPE}=="ntfs", ENV{ID_FS_TYPE}="ntfs3"
EOF

# enable cifs kernel support
echo "cifs" | tee /etc/modules-load.d/cifs.conf

# configure all network cards as DHCP default
cp /root/bootstrap/20-wired.network /etc/systemd/network/20-wired.network
cp /root/bootstrap/20-wireless.network /etc/systemd/network/20-wireless.network

# configure wait online service to wait for only one network to be online
mkdir -p /etc/systemd/system/systemd-networkd-wait-online.service.d
cp /root/bootstrap/wait-online-any.conf \
  /etc/systemd/system/systemd-networkd-wait-online.service.d/wait-online-any.conf

# configure ufw
ufw disable
# outgoing is always allowed, incoming and routed should be denied
ufw default deny incoming
ufw default deny routed
ufw default allow outgoing
# only limited ssh access on all devices
ufw limit ssh comment 'allow limited ssh access'
ufw logging off
ufw enable

# build rmtrash
installdeps rmtrash
su -s /bin/bash - "${TEMPID}" <<EOS
git clone "https://aur.archlinux.org/rmtrash.git"
pushd rmtrash
  git config user.name ""
  git config user.email ""
  git submodule update --init --recursive || true
  makepkg --clean
popd
EOS
pacman -U --needed --noconfirm --color=auto "${TEMPHOME}"/rmtrash/rmtrash-*.pkg.tar.zst

dotnet tool install --global PowerShell --version 7.4.0
su -s /bin/bash - "${USERID}" <<EOS
dotnet tool install --global PowerShell --version 7.4.0
EOS

# create user homes on login
# see https://wiki.archlinux.org/title/LDAP_authentication for more details
tee -a /etc/pam.d/system-login <<EOF
session    required   pam_mkhomedir.so skel=/etc/skel umask=0077
EOF

# == cleanup everything ==
# - tmpusr installation data
if [ -d "${TEMPHOME}" ]; then
  /usr/bin/rm -r "${TEMPHOME}"/* || true
fi
# - package cache
echo -en "y\ny\n" | ( LC_ALL=C pacman -Scc )
# - logs
if [ -f /run/systemd/journal/io.systemd.journal ]; then
  journalctl --rotate
  journalctl --vacuum-time=1s
fi
fstrim --all
# - do not rebuild cache on boot
tee /etc/.updated /var/.updated <<EOF
# For details, see manpage of systemd.unit -> 'ConditionNeedsUpdate'.
TIMESTAMP_NSEC=$(date +%s%N)
EOF
chmod 644 /etc/.updated /var/.updated
