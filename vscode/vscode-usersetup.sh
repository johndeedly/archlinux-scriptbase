#!/usr/bin/env bash

# configure error handling
set -e -o pipefail

# read administrative database to get usernames, group names and home directories
ROOTID=$(getent passwd 0 | cut -d: -f1)
ROOTGRP=$(getent group "$(getent passwd 0 | cut -d: -f4)" | cut -d: -f1)
ROOTHOME=$(getent passwd 0 | cut -d: -f6)
USERID=$(getent passwd 1000 | cut -d: -f1)
USERGRP=$(getent group "$(getent passwd 1000 | cut -d: -f4)" | cut -d: -f1)
USERHOME=$(getent passwd 1000 | cut -d: -f6)
TEMPID=$(getent passwd 4815 | cut -d: -f1)
TEMPGRP=$(getent group "$(getent passwd 4815 | cut -d: -f4)" | cut -d: -f1)
TEMPHOME=$(getent passwd 4815 | cut -d: -f6)
echo "admin: ${ROOTID}|${ROOTGRP}|${ROOTHOME}"
echo "user: ${USERID}|${USERGRP}|${USERHOME}"
echo "temp: ${TEMPID}|${TEMPGRP}|${TEMPHOME}"

# development environment
devenv_install_package_rundeps() {
  pushd ~/.vscode/extensions/$1
    cat package.json \
    | jq -c 'try .runtimeDependencies[] | select((.platforms[] | contains("linux")) and (.architectures[] | (contains("x86_64") or contains("x64"))))' \
    | while read -r line; do
      [ -z "$line" ] && continue
      DESCR=$(echo $line | jq -r '.description')
      URL=$(echo $line | jq -r '.url')
      INSTPATH=$(echo $line | jq -r '.installPath')
      echo "downloading $DESCR"
      echo "from $URL to $INSTPATH"
      wget -O /tmp/file.zip "$URL"
      7z x -aoa -y -o"$INSTPATH" /tmp/file.zip
      pushd "$INSTPATH"
        # fix broken 'Windows' folders in zip
        for file in *\\*; do
          if [ -f "$file" ]; then
            target="${file//\\//}"
            mkdir -p "${target%/*}"
            mv -v "$file" "$target"
          fi
        done
        echo $line | jq -r 'try .binaries[]' |
          while read -r bins; do
            if [[ ! -z "$bins" ]] && [[ -f "$bins" ]]; then
              chmod 0755 "$bins"
            fi
          done
        touch install.Lock
      popd
      rm /tmp/file.zip
    done
  popd
}

# csharp
code --install-extension ms-dotnettools.csharp --force
csharppath=$(ls -1 ~/.vscode/extensions/ | sort -Vr | grep "ms-dotnettools.csharp" | head -1)
devenv_install_package_rundeps "$csharppath"
mkdir -p /tmp/csharp
pushd /tmp/csharp
  # prepare nuget environment for convenient offline usage
  tee csharp.csproj <<EOF
<Project Sdk="Microsoft.NET.Sdk">
  <PropertyGroup>
    <OutputType>Exe</OutputType>
    <TargetFramework>net8.0</TargetFramework>
  </PropertyGroup>
</Project>
EOF
  tee Program.cs <<EOF
using System;
namespace csharp {
  public class Program {
    public static void Main(string[] args) {
      Console.WriteLine("Hello World!");
    }
  }
}
EOF
  dotnet build || true
  dotnet publish -r linux-x64 --self-contained || true
  tee csharp.csproj <<EOF
<Project Sdk="Microsoft.NET.Sdk">
  <PropertyGroup>
    <OutputType>Exe</OutputType>
    <TargetFramework>net6.0</TargetFramework>
  </PropertyGroup>
</Project>
EOF
  dotnet build || true
  dotnet publish -r linux-x64 --self-contained || true
popd
rm -rf /tmp/csharp
# c++
code --install-extension ms-vscode.cpptools --force
csharppath=$(ls -1 ~/.vscode/extensions/ | sort -Vr | grep "ms-vscode.cpptools" | head -1)
devenv_install_package_rundeps "$csharppath"
# python
code --install-extension ms-python.python --force
pip install --upgrade pylint flake8 black pytest pytest-cov \
  wemake-python-styleguide requests cement filelock
curl -sSL https://install.python-poetry.org | python -
poetry --version
poetry self update
# pylance
code --install-extension ms-python.vscode-pylance --force
# xml
code --install-extension dotjoshjohnson.xml --force
# better comments
code --install-extension aaron-bond.better-comments --force
# git graph
code --install-extension mhutchie.git-graph --force
# git blame
code --install-extension waderyan.gitblame --force
# docker
code --install-extension ms-azuretools.vscode-docker --force
# yara
code --install-extension infosec-intern.yara --force
# hex editor
code --install-extension ms-vscode.hexeditor --force
# german language pack
code --install-extension ms-ceintl.vscode-language-pack-de --force
# color code highlighter
code --install-extension naumovs.color-highlight --force
# reset application defaults
#   VS Code claims to be a file manager o_OU -> inode/directory removed from list
[ -f /usr/share/applications/code.desktop ] && \
  xdg-mime default code.desktop `grep 'MimeType=' /usr/share/applications/code.desktop | sed -e 's/.*=//' -e 's/;/ /g' -e 's/inode\/directory//g'`
xdg-mime default code.desktop 'text/plain' 'text/html' 'text/x-shellscript' 'application/json' 'text/xml' 'text/x-php' 'text/x-c' 'text/x-c++'
