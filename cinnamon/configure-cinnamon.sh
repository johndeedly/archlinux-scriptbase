#!/usr/bin/env bash

# configure error handling
set -e -o pipefail

# predefine functions
function installdeps() {
  local TMPPKG=$(mktemp)
  echo "install dependencies for $@"
  while (( "$#" )); do
    curl -sL "https://aur.archlinux.org/cgit/aur.git/plain/PKGBUILD?h=$1" > "${TMPPKG}"
    grep -e 'depends' "${TMPPKG}" && (
      unset depends
      unset makedepends
      unset checkdepends
      source "${TMPPKG}"
      ALLDEPENDS=("${depends[@]}" "${makedepends[@]}" "${checkdepends[@]}")
      for pkg in "${ALLDEPENDS[@]}"; do
        if [ ! -z "${pkg}" ]; then
          pacman -S --needed --noconfirm --asdeps --color=auto "${pkg}" || true
        fi
      done
    ) || echo 1>&2 "no dependencies parsed for $1"
    shift
  done
  /usr/bin/rm "${TMPPKG}"
}

# read administrative database to get usernames, group names and home directories
ROOTID=$(getent passwd 0 | cut -d: -f1)
ROOTGRP=$(getent group "$(getent passwd 0 | cut -d: -f4)" | cut -d: -f1)
ROOTHOME=$(getent passwd 0 | cut -d: -f6)
USERID=$(getent passwd 1000 | cut -d: -f1)
USERGRP=$(getent group "$(getent passwd 1000 | cut -d: -f4)" | cut -d: -f1)
USERHOME=$(getent passwd 1000 | cut -d: -f6)
TEMPID=$(getent passwd 4815 | cut -d: -f1)
TEMPGRP=$(getent group "$(getent passwd 4815 | cut -d: -f4)" | cut -d: -f1)
TEMPHOME=$(getent passwd 4815 | cut -d: -f6)
echo "admin: ${ROOTID}|${ROOTGRP}|${ROOTHOME}"
echo "user: ${USERID}|${USERGRP}|${USERHOME}"
echo "temp: ${TEMPID}|${TEMPGRP}|${TEMPHOME}"

# install cinnamon package
pacman -S --noconfirm --color=auto cinnamon cinnamon-translations networkmanager system-config-printer

# gnome-icon-theme-symbolic
installdeps gnome-icon-theme-symbolic
su -s /bin/bash - "${TEMPID}" <<EOS
git clone "https://aur.archlinux.org/gnome-icon-theme-symbolic.git"
pushd gnome-icon-theme-symbolic
  git config user.name ""
  git config user.email ""
  git submodule update --init --recursive || true
  makepkg --clean
popd
EOS
pacman -U --needed --noconfirm --color=auto "${TEMPHOME}"/gnome-icon-theme-symbolic/gnome-icon-theme-symbolic-*.pkg.tar.zst

# gnome-icon-theme
installdeps gnome-icon-theme
pacman -S --needed --noconfirm --asdeps --color=auto intltool icon-naming-utils gtk-update-icon-cache hicolor-icon-theme
su -s /bin/bash - "${TEMPID}" <<EOS
git clone "https://aur.archlinux.org/gnome-icon-theme.git"
pushd gnome-icon-theme
  git config user.name ""
  git config user.email ""
  git submodule update --init --recursive || true
  makepkg --clean
popd
EOS
pacman -U --needed --noconfirm --color=auto "${TEMPHOME}"/gnome-icon-theme/gnome-icon-theme-*.pkg.tar.zst

# configure cinnamon to be the default window manager
sed -i 's/XDG_CURRENT_DESKTOP=.*/XDG_CURRENT_DESKTOP="cinnamon"/' /etc/skel/.xinitrc
tee -a /etc/skel/.xinitrc <<EOF

exec cinnamon-session --session cinnamon
EOF
cp /etc/skel/.xinitrc "${USERHOME}"/.xinitrc
cp /etc/skel/.xinitrc "${ROOTHOME}"/.xinitrc

# configure additional application defaults and settings
systemctl enable NetworkManager.service
su - "${USERID}" <<EOS
XDG_CONFIG_HOME="${USERHOME}"/.config dconf dump /org/cinnamon/ > "${USERHOME}"/dconf-dump.ini
tee -a "${USERHOME}"/dconf-dump.ini <<EOX

[/]
favorite-apps=['firefox.desktop', 'chromium.desktop', 'kitty.desktop', 'cinnamon-settings.desktop', 'nemo.desktop']

[desktop/background]
picture-uri='file:///usr/share/backgrounds/elementaryos-default'
picture-options='zoom'
primary-color='000000'
secondary-color='000000'
draw-background=true

[desktop/applications/calculator]
exec='qalculate-gtk'

[desktop/applications/terminal]
exec='kitty'
exec-arg='--'

[desktop/keybindings]
custom-list=['__dummy__', 'custom0', 'custom1', 'custom2', 'custom3']
looking-glass-keybinding=@as []
pointer-next-monitor=@as []
pointer-previous-monitor=@as []
show-desklets=@as []

[desktop/keybindings/custom-keybindings/custom0]
binding=['<Shift><Super>Return']
command='wofi --fork --normal-window --insensitive --allow-images --allow-markup --show drun'
name='wofi'

[desktop/keybindings/custom-keybindings/custom1]
binding=['<Super>p', 'XF86Display']
command='arandr'
name='arandr'

[desktop/keybindings/custom-keybindings/custom2]
binding=['<Alt>e']
command='kitty /usr/bin/lf'
name='lf'

[desktop/keybindings/custom-keybindings/custom3]
binding=['<Alt>w']
command='chromium'
name='chromium'

[desktop/keybindings/media-keys]
calculator=@as []
email=@as []
home=['<Super>e']
screensaver=['<Super>l', 'XF86ScreenSaver']
search=@as []
terminal=['<Super>Return']
www=['<Super>w']

[desktop/keybindings/wm]
activate-window-menu=@as []
begin-move=@as []
begin-resize=@as []
close=['<Super>q']
move-to-monitor-down=['<Shift><Alt>Down']
move-to-monitor-left=['<Shift><Alt>Left']
move-to-monitor-right=['<Shift><Alt>Right']
move-to-monitor-up=['<Shift><Alt>Up']
move-to-workspace-1=['<Shift><Super>1']
move-to-workspace-2=['<Shift><Super>2']
move-to-workspace-3=['<Shift><Super>3']
move-to-workspace-4=['<Shift><Super>4']
move-to-workspace-5=['<Shift><Super>5']
move-to-workspace-6=['<Shift><Super>6']
move-to-workspace-7=['<Shift><Super>7']
move-to-workspace-8=['<Shift><Super>8']
move-to-workspace-9=['<Shift><Super>9']
move-to-workspace-10=@as []
move-to-workspace-11=@as []
move-to-workspace-12=@as []
move-to-workspace-down=['<Shift><Super>Down']
move-to-workspace-left=['<Shift><Super>Left']
move-to-workspace-right=['<Shift><Super>Right']
move-to-workspace-up=['<Shift><Super>Up']
panel-run-dialog=@as []
push-tile-down=['<Ctrl><Super>Down']
push-tile-left=['<Ctrl><Super>Left']
push-tile-right=['<Ctrl><Super>Right']
push-tile-up=['<Ctrl><Super>Up']
show-desktop=@as []
switch-group=@as []
switch-group-backward=@as []
switch-monitor=@as []
switch-panels=@as []
switch-panels-backward=@as []
switch-to-workspace-1=['<Super>1']
switch-to-workspace-2=['<Super>2']
switch-to-workspace-3=['<Super>3']
switch-to-workspace-4=['<Super>4']
switch-to-workspace-5=['<Super>5']
switch-to-workspace-6=['<Super>6']
switch-to-workspace-7=['<Super>7']
switch-to-workspace-8=['<Super>8']
switch-to-workspace-9=['<Super>9']
switch-to-workspace-10=@as []
switch-to-workspace-11=@as []
switch-to-workspace-12=@as []
switch-to-workspace-left=['<Super>Left']
switch-to-workspace-right=['<Super>Right']
switch-windows=['<Super>Tab']
switch-windows-backward=['<Shift><Super>Tab']
toggle-maximized=['<Super>f']
unmaximize=@as []
EOX
dbus-run-session -- bash -c 'XDG_CONFIG_HOME="${USERHOME}"/.config dconf load /org/cinnamon/ < "${USERHOME}"/dconf-dump.ini'
/usr/bin/rm "${USERHOME}"/dconf-dump.ini

[ -f /usr/share/applications/nemo.desktop ] && \
  xdg-mime default nemo.desktop `grep 'MimeType=' /usr/share/applications/nemo.desktop | sed -e 's/.*=//' -e 's/;/ /g'`
EOS

# create skeleton for autostart desktop files
# cp /root/cinnamon/dunst.desktop /etc/xdg/autostart/dunst.desktop
# chmod +x /etc/xdg/autostart/dunst.desktop
cp /root/cinnamon/flameshot.desktop /etc/xdg/autostart/flameshot.desktop
chmod +x /etc/xdg/autostart/flameshot.desktop
cp /root/cinnamon/gammastep.desktop /etc/xdg/autostart/gammastep.desktop
chmod +x /etc/xdg/autostart/gammastep.desktop
# cp /root/cinnamon/wallpaper.desktop /etc/xdg/autostart/wallpaper.desktop
# chmod +x /etc/xdg/autostart/wallpaper.desktop
cp /root/cinnamon/wallpaper.service /etc/systemd/user/wallpaper.service
cp /root/cinnamon/wallpaper.sh /usr/bin/wallpaper.sh
chmod +x /usr/bin/wallpaper.sh
systemctl --global enable wallpaper.service

# create skeleton for desktop configuration files
arrVar=( "calendar" "grouped-window-list" "menu" "network" "notifications" "sound" )
for item in "${arrVar[@]}"; do
  mkdir -p "/etc/skel/.config/cinnamon/spices/${item}@cinnamon.org"
  cp "/root/cinnamon/${item}@cinnamon.org/"*.json "/etc/skel/.config/cinnamon/spices/${item}@cinnamon.org/"
  mkdir -p "${USERHOME}/.config/cinnamon/spices/${item}@cinnamon.org"
  cp "/root/cinnamon/${item}@cinnamon.org/"*.json "${USERHOME}/.config/cinnamon/spices/${item}@cinnamon.org/"
  mkdir -p "${ROOTHOME}/.config/cinnamon/spices/${item}@cinnamon.org"
  cp "/root/cinnamon/${item}@cinnamon.org/"*.json "${ROOTHOME}/.config/cinnamon/spices/${item}@cinnamon.org/"
done
chown -R "${USERID}:${USERGRP}" "${USERHOME}/.config/cinnamon"
unset item
unset arrVar

# configure ly to have username and cinnamon prefilled on first boot
tee /etc/ly/save <<EOF
${USERID}
3
EOF

# == cleanup everything ==
# - tmpusr installation data
if [ -d "${TEMPHOME}" ]; then
  /usr/bin/rm -r "${TEMPHOME}"/* || true
fi
# - package cache
echo -en "y\ny\n" | ( LC_ALL=C pacman -Scc )
# - logs
if [ -f /run/systemd/journal/io.systemd.journal ]; then
  journalctl --rotate
  journalctl --vacuum-time=1s
fi
fstrim --all
# - do not rebuild cache on boot
tee /etc/.updated /var/.updated <<EOF
# For details, see manpage of systemd.unit -> 'ConditionNeedsUpdate'.
TIMESTAMP_NSEC=$(date +%s%N)
EOF
chmod 644 /etc/.updated /var/.updated
