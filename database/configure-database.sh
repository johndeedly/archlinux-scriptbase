#!/usr/bin/env bash

# configure error handling
set -e -o pipefail

# read administrative database to get usernames, group names and home directories
ROOTID=$(getent passwd 0 | cut -d: -f1)
ROOTGRP=$(getent group "$(getent passwd 0 | cut -d: -f4)" | cut -d: -f1)
ROOTHOME=$(getent passwd 0 | cut -d: -f6)
USERID=$(getent passwd 1000 | cut -d: -f1)
USERGRP=$(getent group "$(getent passwd 1000 | cut -d: -f4)" | cut -d: -f1)
USERHOME=$(getent passwd 1000 | cut -d: -f6)
TEMPID=$(getent passwd 4815 | cut -d: -f1)
TEMPGRP=$(getent group "$(getent passwd 4815 | cut -d: -f4)" | cut -d: -f1)
TEMPHOME=$(getent passwd 4815 | cut -d: -f6)
echo "admin: ${ROOTID}|${ROOTGRP}|${ROOTHOME}"
echo "user: ${USERID}|${USERGRP}|${USERHOME}"
echo "temp: ${TEMPID}|${TEMPGRP}|${TEMPHOME}"

pacman -S --needed --noconfirm postgresql
systemctl enable postgresql
mkdir /run/postgresql && chown postgres:postgres /run/postgresql

su -s /bin/bash - postgres <<EOS
initdb --locale=C.UTF-8 --encoding=UTF8 -D /var/lib/postgres/data --data-checksums
EOS

cp /root/database/pg_hba.conf /var/lib/postgres/data/pg_hba.conf
chown postgres:postgres /var/lib/postgres/data/pg_hba.conf
chmod 0600 /var/lib/postgres/data/pg_hba.conf

su -s /bin/bash - postgres <<EOS
sed -i '0,/^#\?listen_addresses.*/s//listen_addresses = '"'"'*'"'"'/' /var/lib/postgres/data/postgresql.conf
pg_ctl -w -D /var/lib/postgres/data start
createuser -w localuser
createdb localdatabase
psql -c 'GRANT ALL PRIVILEGES ON DATABASE localdatabase TO localuser;'
psql -c 'CREATE TABLE public."Logs" ( "Message" text NULL, "MessageTemplate" text NULL, "Level" varchar NULL, "RaiseDate" timestamptz NULL, "Exception" text NULL, "LogEvent" jsonb NULL, "Properties" jsonb NULL );' localdatabase
psql -c 'CREATE EXTENSION pg_trgm;' localdatabase
psql -c 'CREATE INDEX "Logs_Message_idx" ON "Logs" USING GIN ("Message" gin_trgm_ops);' localdatabase
psql -c 'CREATE INDEX "Logs_Level_idx" ON "Logs" ("Level");' localdatabase
psql -c 'CREATE INDEX "Logs_RaiseDate_idx" ON "Logs" ("RaiseDate");' localdatabase
psql -c 'CREATE INDEX "Logs_Exception_idx" ON "Logs" USING GIN ("Exception" gin_trgm_ops);' localdatabase
psql -c 'CREATE INDEX "Logs_LogEvent_idx" ON "Logs" USING GIN ("LogEvent" jsonb_path_ops);' localdatabase
psql -c 'CREATE INDEX "Logs_Properties_idx" ON "Logs" USING GIN ("Properties" jsonb_path_ops);' localdatabase
psql -c 'ALTER DEFAULT PRIVILEGES IN SCHEMA public GRANT ALL ON TABLES TO localuser;' localdatabase
psql -c 'GRANT ALL PRIVILEGES ON SCHEMA public TO localuser;' localdatabase
psql -c 'ALTER TABLE "Logs" OWNER TO localuser;' localdatabase
psql -c 'GRANT ALL PRIVILEGES ON TABLE "Logs" TO localuser;' localdatabase
pg_ctl -w -D /var/lib/postgres/data stop
EOS

ufw disable
ufw allow in on lo to any port 5432 comment 'postgresql unlimited on loopback'
ufw limit 5432 comment 'postgresql limited on other interfaces'
ufw enable

# == cleanup everything ==
# - tmpusr installation data
if [ -d "${TEMPHOME}" ]; then
  /usr/bin/rm -r "${TEMPHOME}"/* || true
fi
# - package cache
echo -en "y\ny\n" | ( LC_ALL=C pacman -Scc )
# - logs
if [ -f /run/systemd/journal/io.systemd.journal ]; then
  journalctl --rotate
  journalctl --vacuum-time=1s
fi
fstrim --all
# - do not rebuild cache on boot
tee /etc/.updated /var/.updated <<EOF
# For details, see manpage of systemd.unit -> 'ConditionNeedsUpdate'.
TIMESTAMP_NSEC=$(date +%s%N)
EOF
chmod 644 /etc/.updated /var/.updated
