#!/usr/bin/env bash

# configure error handling
set -e -o pipefail

# read administrative database to get usernames, group names and home directories
ROOTID=$(getent passwd 0 | cut -d: -f1)
ROOTGRP=$(getent group "$(getent passwd 0 | cut -d: -f4)" | cut -d: -f1)
ROOTHOME=$(getent passwd 0 | cut -d: -f6)
USERID=$(getent passwd 1000 | cut -d: -f1)
USERGRP=$(getent group "$(getent passwd 1000 | cut -d: -f4)" | cut -d: -f1)
USERHOME=$(getent passwd 1000 | cut -d: -f6)
TEMPID=$(getent passwd 4815 | cut -d: -f1)
TEMPGRP=$(getent group "$(getent passwd 4815 | cut -d: -f4)" | cut -d: -f1)
TEMPHOME=$(getent passwd 4815 | cut -d: -f6)
echo "admin: ${ROOTID}|${ROOTGRP}|${ROOTHOME}"
echo "user: ${USERID}|${USERGRP}|${USERHOME}"
echo "temp: ${TEMPID}|${TEMPGRP}|${TEMPHOME}"

# create podman user
groupadd podman
useradd -m -g podman podman
# - loginctl enable-linger podman
mkdir -p /var/lib/systemd/linger
touch /var/lib/systemd/linger/podman

# install packages
pacman -S --needed --noconfirm --color=auto podman buildah fuse-overlayfs
su -s /bin/bash - podman <<EOS
podman system migrate
podman info
EOS

# == cleanup everything ==
# - tmpusr installation data
if [ -d "${TEMPHOME}" ]; then
  /usr/bin/rm -r "${TEMPHOME}"/* || true
fi
# - package cache
echo -en "y\ny\n" | ( LC_ALL=C pacman -Scc )
# - logs
if [ -f /run/systemd/journal/io.systemd.journal ]; then
  journalctl --rotate
  journalctl --vacuum-time=1s
fi
fstrim --all
# - do not rebuild cache on boot
tee /etc/.updated /var/.updated <<EOF
# For details, see manpage of systemd.unit -> 'ConditionNeedsUpdate'.
TIMESTAMP_NSEC=$(date +%s%N)
EOF
chmod 644 /etc/.updated /var/.updated
