#!/usr/bin/env bash

# initialize root
USER="$(id -un)"
if [ ! "$EUID" -eq 0 ]; then
  sudo "$0" "$@"
  exit $?
else
  echo "Running as user '$USER'"
fi

# parse parameters
DEVICE=""
ENCRYPTION=""
YES=""
POSITIONAL=()
while [ $# -gt 0 ]; do
  key="$1"
  case $key in
    -h|--help)
      shift
      echo "TODO"
      exit 1
      ;;
    -d|--device)
      DEVICE="$2"
      shift
      shift
      echo "[#] using DEVICE=${DEVICE}"
      ;;
    -e|--encryption)
      ENCRYPTION="$2"
      shift
      shift
      echo "[#] using ENCRYPTION=${ENCRYPTION}"
      ;;
    -y|--yes)
      YES="YES"
      shift
      echo "[#] assume yes on every option"
      ;;
    *)
      POSITIONAL+=("$1") # save it in an array for later
      shift
      ;;
  esac
done
set -- "${POSITIONAL[@]}" # restore positional parameters

# resize tmpfs
mount -o remount,size=80% /run/archiso/cowspace

# read system setup
readarray -t devicelist < <(lsblk -dplnx size -o name,size | grep -Ev "boot|rpmb|loop" | tac)
if [ -z "${DEVICE}" ]; then
  echo "Select installation disk:"
  select opt in "${devicelist[@]}"
  do
    if ! [ -z "$opt" ]; then
      DEVICE=$(echo "$opt" | cut -d' ' -f1)
      break
    fi
  done
fi

# partition the selected disk
parted -s -a optimal -- ${DEVICE} \
  mklabel gpt \
  mkpart work btrfs 4MiB -4MiB

sleep 5

part_work="$(ls ${DEVICE}* | grep -E "^${DEVICE}p?1$")"

# force unmount partitions when running the script a second time
fuser -km ${part_work} || true

# create file systems and configuration space for grub
dd if=/dev/zero of=${part_work} bs=1M count=16 iflag=fullblock status=progress
mkfs.btrfs -L WORK ${part_work}

mkdir -m700 -p /install
mount --mkdir ${part_work} /install
btrfs subvol create /install/@
umount /install
mount -o subvol=@,compress=zstd:4,noatime ${part_work} /install

# enabling ufw filters inside container
modprobe iptable_filter
modprobe ip6table_filter

pacstrap -K -c /install base archiso
cp -r /root/* /install/root/
ls -l /install/root/

systemd-nspawn --capability=CAP_SYS_CHROOT,CAP_NET_ADMIN,CAP_NET_RAW --pipe -D /install <<EOX
pushd /root
  cp -r /usr/share/archiso/configs/releng/ custom
  
  # copy scriptbase to image
  cp -r bootstrap custom/airootfs/root/
  cp -r cinnamon custom/airootfs/root/
  cp -r database custom/airootfs/root/
  cp -r debugger custom/airootfs/root/
  cp -r graphical custom/airootfs/root/
  cp -r localmirror custom/airootfs/root/
  cp -r mailrelay custom/airootfs/root/
  cp -r podman-hub custom/airootfs/root/
  cp -r qtile custom/airootfs/root/
  cp -r router custom/airootfs/root/
  cp -r vscode custom/airootfs/root/
  cp finalize*.sh custom/airootfs/root/
  chmod +x custom/airootfs/root/finalize*.sh
  cp install*.sh custom/airootfs/root/
  chmod +x custom/airootfs/root/install*.sh
  cp prepare*.sh custom/airootfs/root/
  chmod +x custom/airootfs/root/prepare*.sh
  
  # disable reflector.service
  rm custom/airootfs/etc/systemd/system/multi-user.target.wants/reflector.service

#  # set root user password
#  tee custom/airootfs/etc/shadow <<EOF
#root:$( echo "toor" | openssl passwd -6 -stdin ):14871::::::
#EOF

  # change iso date string to 'custom'
  sed -i 's/^iso_version=.*/iso_version="custom"/g' custom/profiledef.sh

  # i18n
  tee custom/airootfs/etc/vconsole.conf <<EOF
KEYMAP=de
XKBLAYOUT=de
XKBMODEL=pc105
EOF

  # add some packages
  tee -a custom/packages.x86_64 <<EOF
buildah
podman
fuse-overlayfs
EOF

  # build the image
  mkdir -p work out
  mkarchiso -v -w work -o out custom
popd
EOX

cp /install/root/out/archlinux-custom-x86_64.iso /share/
