#!/usr/bin/env bash

# initialize root
USER="$(id -un)"
if [ ! "$EUID" -eq 0 ]; then
  sudo "$0" "$@"
  exit $?
else
  echo "Running as user '$USER'"
fi

# parse parameters
DEVICE=""
ENCRYPTION=""
YES=""
POSITIONAL=()
while [ $# -gt 0 ]; do
  key="$1"
  case $key in
    -h|--help)
      shift
      echo "TODO"
      exit 1
      ;;
    -y|--yes)
      YES="YES"
      shift
      echo "[#] assume yes on every option"
      ;;
    *)
      POSITIONAL+=("$1") # save it in an array for later
      shift
      ;;
  esac
done
set -- "${POSITIONAL[@]}" # restore positional parameters

export TMPDIR="/install/tmp"
echo "[#] podman info" && podman info
echo "[#] buildah info" && buildah info

fuser -km /mnt || true
sync
umount /mnt || true
buildah umount worker

pushd /install
  buildah commit worker worker
  podman save -o worker.tar worker
  gzip -k worker.tar
  ls -l --si worker.tar worker.tar.gz
popd
