#!/usr/bin/env bash

# initialize root
USER="$(id -un)"
if [ ! "$EUID" -eq 0 ]; then
  sudo "$0" "$@"
  exit $?
else
  echo "Running as user '$USER'"
fi

# parse parameters
DEVICE=""
ENCRYPTION=""
DUALBOOT=""
YES=""
POSITIONAL=()
while [ $# -gt 0 ]; do
  key="$1"
  case $key in
    -h|--help)
      shift
      echo "TODO"
      exit 1
      ;;
    -d|--device)
      DEVICE="$2"
      shift
      shift
      echo "[#] using DEVICE=${DEVICE}"
      ;;
    -e|--encryption)
      ENCRYPTION="$2"
      shift
      shift
      echo "[#] using ENCRYPTION=${ENCRYPTION}"
      ;;
    -b|--dualboot)
      DUALBOOT="YES"
      shift
      echo "[#] dualboot partition setup"
      ;;
    -y|--yes)
      YES="YES"
      shift
      echo "[#] assume yes on every option"
      ;;
    *)
      POSITIONAL+=("$1") # save it in an array for later
      shift
      ;;
  esac
done
set -- "${POSITIONAL[@]}" # restore positional parameters

# resize tmpfs
mount -o remount,size=80% /run/archiso/cowspace

# read system setup
readarray -t devicelist < <(lsblk -dplnx size -o name,size | grep -Ev "boot|rpmb|loop" | tac)
if [ -z "${DEVICE}" ]; then
  echo "Select installation disk:"
  select opt in "${devicelist[@]}"
  do
    if ! [ -z "$opt" ]; then
      DEVICE=$(echo "$opt" | cut -d' ' -f1)
      break
    fi
  done
fi

# calculate swap size
ramsize=$(($(grep MemTotal /proc/meminfo | awk '{print $2}') / 1024))
if [ ${ramsize} -gt 8192 ]; then
  swapsize=10240
else
  swapsize=$((${ramsize} * 5 / 4))
fi

# making sure everything is understood
if [ -z "${DUALBOOT}" ]; then
part1=4
part2=$( expr ${part1} + 512 )
part3=$( expr ${part2} + \( 48 \* 1024 \) )
echo -e "The partition table will be:\n\n\
${DEVICE} [gpt]\n\
├─[efi]        fat32     ${part1}MiB ─> ${part2}MiB\n\
│ └─/boot\n\
├─[root]       btrfs     ${part2}MiB ─> ${part3}MiB\n\
│ └─/          └─@\n\
└─lvm (luks)             ${part3}MiB ─> -4MiB\n\
  ├─[swap]     swap      ├─0MiB ─> ${swapsize}MiB\n\
  └─[data]     btrfs     └─${swapsize}MiB ─> -0MiB\n\
    ├─/home    ├─@home\n\
    ├─/var     ├─@var\n\
    ├─/root    ├─@root\n\
    ├─/srv     ├─@srv\n\
    └─/opt     └─@opt\n"
else
# |---------------total----------------|
# |-first parts-|------data space------|
# |-------------|--windows--|--linux---|
totalmib=$( expr `blockdev --getsize64 "${DEVICE}"` / 1024 / 1024 )
part1=4
part2=$( expr ${part1} + 512 )
part3=$( expr ${part2} + 128 )
part4=$( expr ${part3} + \( 96 \* 1024 \) )
part5=$( expr ${part4} + 1024 )
part6=$( expr ${part5} + \( 48 \* 1024 \) )
# 4 MiB at the end to be on the safe side
dataspace=$( expr ${totalmib} - ${part6} - 4 )
# "/ 4 * 2" is very important here!! 4 MiB boundaries for partitions
halfdata=$( expr ${dataspace} / 4 \* 2 )
part7=$( expr ${part6} + ${halfdata} )
echo -e "The partition table will be:\n\n\
${DEVICE} [gpt]\n\
├─[efi]        fat32     ${part1}MiB ─> ${part2}MiB\n\
│ └─/boot\n\
├─[msres]                ${part2}MiB ─> ${part3}MiB\n\
├─[win]        ntfs      ${part3}MiB ─> ${part4}MiB\n\
├─[msdiag]     ntfs      ${part4}MiB ─> ${part5}MiB\n\
├─[root]       btrfs     ${part5}MiB ─> ${part6}MiB\n\
│ └─/          └─@\n\
├─[windata]    ntfs      ${part6}MiB ─> ${part7}MiB\n\
└─lvm (luks)             ${part7}MiB ─> -4MiB\n\
  ├─[swap]     swap      ├─0MiB ─> ${swapsize}MiB\n\
  └─[data]     btrfs     └─${swapsize}MiB ─> -0MiB\n\
    ├─/home    ├─@home\n\
    ├─/var     ├─@var\n\
    ├─/root    ├─@root\n\
    ├─/srv     ├─@srv\n\
    └─/opt     └─@opt\n"
fi
if [ -z "$YES" ]; then
  options=("yes" "no")
  select opt in "${options[@]}"
  do
    case $opt in
      "yes")
        break
        ;;
      "no")
        clear
        echo >&2 "aborted"
        exit 1
        ;;
      *)
        ;;
    esac
  done
fi

# partition the selected disk
data_id="data"
volgrp_id="volgrp"

if [ -z "${DUALBOOT}" ]; then
parted -s -a optimal -- ${DEVICE} \
  mklabel gpt \
  mkpart efi fat32 ${part1}MiB ${part2}MiB \
  set 1 esp on \
  mkpart root btrfs ${part2}MiB ${part3}MiB \
  mkpart ${data_id} ${part3}MiB -4MiB
else
parted -s -a optimal -- ${DEVICE} \
  mklabel gpt \
  mkpart efi fat32 ${part1}MiB ${part2}MiB \
  set 1 esp on \
  mkpart msr ${part2}MiB ${part3}MiB \
  set 2 msftres on \
  mkpart win ntfs ${part3}MiB ${part4}MiB \
  set 3 msftdata on \
  mkpart diag ${part4}MiB ${part5}MiB \
  set 4 diag on \
  mkpart root btrfs ${part5}MiB ${part6}MiB \
  mkpart win${data_id} ${part6}MiB ${part7}MiB \
  set 6 msftdata on \
  mkpart ${data_id} ${part7}MiB -4MiB
fi

sleep 5

part_efi="$(ls ${DEVICE}* | grep -E "^${DEVICE}p?1$")"
if [ -z "${DUALBOOT}" ]; then
  part_root="$(ls ${DEVICE}* | grep -E "^${DEVICE}p?2$")"
  part_data="$(ls ${DEVICE}* | grep -E "^${DEVICE}p?3$")"
else
  part_msr="$(ls ${DEVICE}* | grep -E "^${DEVICE}p?2$")"
  part_win="$(ls ${DEVICE}* | grep -E "^${DEVICE}p?3$")"
  part_diag="$(ls ${DEVICE}* | grep -E "^${DEVICE}p?4$")"
  part_root="$(ls ${DEVICE}* | grep -E "^${DEVICE}p?5$")"
  part_windata="$(ls ${DEVICE}* | grep -E "^${DEVICE}p?6$")"
  part_data="$(ls ${DEVICE}* | grep -E "^${DEVICE}p?7$")"
fi

parted -s -a optimal -- ${DEVICE} \
  unit MiB print free

# force unmount partitions when running the script a second time
fuser -km ${part_efi} || true
if [ -z "${DUALBOOT}" ]; then
  fuser -km ${part_root} || true
  fuser -km ${part_data} || true
else
  fuser -km ${part_msr} || true
  fuser -km ${part_win} || true
  fuser -km ${part_diag} || true
  fuser -km ${part_root} || true
  fuser -km ${part_data} || true
fi

# ask for the encryption password if "YES" is not set
if [ -z "${ENCRYPTION}" ]; then
  if [ -z "${YES}" ]; then
    echo "Enter password for encrypted ${data_id} partition"
    ENCRYPTION=$(ask_passwd "(leave empty for no encryption)")
  fi
fi

# create file systems and configuration space for grub
dd if=/dev/zero of=${part_data} bs=1M count=32 iflag=fullblock status=progress
if ! [ -z "$ENCRYPTION" ]; then
  echo -n "${ENCRYPTION}" | (cryptsetup -q -v luksFormat ${part_data} -d -)
  echo -n "${ENCRYPTION}" | (cryptsetup -q open ${part_data} ${data_id} -d -)
  pvcreate -f /dev/mapper/${data_id}
  vgcreate -f ${volgrp_id} /dev/mapper/${data_id}
else
  pvcreate -f ${part_data}
  vgcreate -f ${volgrp_id} ${part_data}
fi
lvcreate -L ${swapsize}M ${volgrp_id} -n swap
lvcreate -l 100%FREE ${volgrp_id} -n ${data_id}

dd if=/dev/zero of=${part_efi} bs=1M count=16 iflag=fullblock status=progress
mkfs.fat -F32 -n EFI ${part_efi}
if [ ! -z "${DUALBOOT}" ]; then
  dd if=/dev/zero of=${part_msr} bs=1M count=16 iflag=fullblock status=progress
  dd if=/dev/zero of=${part_win} bs=1M count=16 iflag=fullblock status=progress
  mkfs.ntfs -f -L WINDOWS ${part_win}
  dd if=/dev/zero of=${part_diag} bs=1M count=16 iflag=fullblock status=progress
  mkfs.ntfs -f -L WINRE ${part_diag}
fi
dd if=/dev/zero of=${part_root} bs=1M count=16 iflag=fullblock status=progress
mkfs.btrfs -L ROOT ${part_root}
if [ ! -z "${DUALBOOT}" ]; then
  dd if=/dev/zero of=${part_windata} bs=1M count=16 iflag=fullblock status=progress
  mkfs.ntfs -f -L WINDATA ${part_windata}
fi
dd if=/dev/zero of=/dev/${volgrp_id}/swap bs=1M count=16 iflag=fullblock status=progress
mkswap -L SWAP /dev/${volgrp_id}/swap
swapon /dev/${volgrp_id}/swap
dd if=/dev/zero of=/dev/${volgrp_id}/${data_id} bs=1M count=16 iflag=fullblock status=progress
mkfs.btrfs -L DATA /dev/${volgrp_id}/${data_id}

mkdir -m700 -p /install
mount --mkdir ${part_root} /install
btrfs subvol create /install/@
#btrfs subvol create /install/@.snapshots
umount /install
mount -o subvol=@,compress=zstd:4,noatime ${part_root} /install
#mkdir -m750 /install/.snapshots
#mount -o subvol=@.snapshots,compress=zstd:4,noatime ${part_root} /install/.snapshots
#chmod 750 /install/.snapshots
mkdir -p /install/boot
mount ${part_efi} /install/boot
# protect /${data_id} and /mnt from being writable to root user (used only as a mount point, protection against filling up your '/' on mount errors)
mkdir -m000 /install/mnt
chattr +i /install/mnt
mount /dev/${volgrp_id}/${data_id} /install/mnt
chmod 750 /install/mnt
#btrfs subvol create /install/mnt/@
btrfs subvol create /install/mnt/@home
btrfs subvol create /install/mnt/@var
btrfs subvol create /install/mnt/@root
btrfs subvol create /install/mnt/@srv
btrfs subvol create /install/mnt/@opt
#btrfs subvol create /install/${data_id}/@.snapshots
umount /install/mnt
#mount -o subvol=@,compress=zstd:4,noatime /dev/${volgrp_id}/${data_id} /install/${data_id}
#chmod 750 /install/${data_id}

mkdir -m755 /install/home
mount -o subvol=@home,compress=zstd:4,noatime /dev/${volgrp_id}/${data_id} /install/home
chmod 755 /install/home
mkdir -m755 /install/var
mount -o subvol=@var,compress=zstd:4,noatime /dev/${volgrp_id}/${data_id} /install/var
chmod 755 /install/var
mkdir -m750 /install/root
mount -o subvol=@root,compress=zstd:4,noatime /dev/${volgrp_id}/${data_id} /install/root
chmod 750 /install/root
mkdir -m755 /install/srv
mount -o subvol=@srv,compress=zstd:4,noatime /dev/${volgrp_id}/${data_id} /install/srv
chmod 755 /install/srv
mkdir -m755 /install/opt
mount -o subvol=@opt,compress=zstd:4,noatime /dev/${volgrp_id}/${data_id} /install/opt
chmod 755 /install/opt
#mkdir -m750 /install/${data_id}/.snapshots
#mount -o subvol=@.snapshots,compress=zstd:4,noatime /dev/${volgrp_id}/${data_id} /install/${data_id}/.snapshots
#chmod 750 /install/${data_id}/.snapshots

mkdir -m755 /install/etc
genfstab -U -p /install > /install/etc/fstab

# bugfix for genfstab regarding bind mounts not stripping /install in front of path
sed -i 's/\/install\/'"${data_id}"'\//\/'"${data_id}"'\//g' /install/etc/fstab

# enable qemu shared folder for host.0 bindmount
if mountpoint -q -- /share; then
  mkdir -m777 /install/share
  tee -a /install/etc/fstab << EOF

# host.0 qemu share
host.0    /share    9p    trans=virtio,version=9p2000.L,rw
EOF
fi

if ! [ -z "$ENCRYPTION" ]; then
  root_uuid=$(blkid ${part_root} -s UUID -o value)
  crypt_uuid=$(cryptsetup luksUUID ${part_data})
  echo "${data_id} /dev/disk/by-uuid/${crypt_uuid} none luks" > /install/etc/crypttab
  # place encryption key at /boot to allow auto decryption
  echo -n "${ENCRYPTION}" > /install/boot/luks.key
fi

# print final partition layout
lsblk -o NAME,TYPE,FSTYPE,SIZE,FSAVAIL ${DEVICE}

# cleaning up
sleep 5
sync
umount -R /install
/usr/bin/rm -rf /install

# mount everything in place
mount -o subvol=@,compress=zstd:4,noatime ${part_root} /mnt
#mount -o subvol=@.snapshots,compress=zstd:4,noatime ${part_root} /mnt/.snapshots
mount ${part_efi} /mnt/boot

#mount -o subvol=@,compress=zstd:4,noatime /dev/${volgrp_id}/${data_id} /mnt/data
mount -o subvol=@home,compress=zstd:4,noatime /dev/${volgrp_id}/${data_id} /mnt/home
mount -o subvol=@var,compress=zstd:4,noatime /dev/${volgrp_id}/${data_id} /mnt/var
mount -o subvol=@root,compress=zstd:4,noatime /dev/${volgrp_id}/${data_id} /mnt/root
mount -o subvol=@srv,compress=zstd:4,noatime /dev/${volgrp_id}/${data_id} /mnt/srv
mount -o subvol=@opt,compress=zstd:4,noatime /dev/${volgrp_id}/${data_id} /mnt/opt
#mount -o subvol=@.snapshots,compress=zstd:4,noatime /dev/${volgrp_id}/${data_id} /mnt/data/.snapshots

# enabling ufw filters inside container
modprobe iptable_filter
modprobe ip6table_filter

# systemd 254.3-1 fixes the problem -> base readded
#
# ==> FS#79619 - [systemd] 20-systemd-sysusers.hook fails to execute on a fresh system
# ==> https://bugs.archlinux.org/task/79619
#
pacstrap -K -M -c /mnt base linux linux-headers linux-lts linux-lts-headers \
  linux-firmware efibootmgr dosfstools mtools lvm2 btrfs-progs

# no fallback initcpio as of lts being used as fallback
if [ -f /mnt/usr/share/mkinitcpio/hook.preset ]; then
  sed -i "s/^PRESETS=.*/PRESETS=('default')/g" /mnt/usr/share/mkinitcpio/hook.preset
fi
if [ -f /mnt/etc/mkinitcpio.d/linux.preset ]; then
  sed -i "s/^PRESETS=.*/PRESETS=('default')/g" /mnt/etc/mkinitcpio.d/linux.preset
fi
if [ -f /mnt/etc/mkinitcpio.d/linux-lts.preset ]; then
  sed -i "s/^PRESETS=.*/PRESETS=('default')/g" /mnt/etc/mkinitcpio.d/linux-lts.preset
fi
/usr/bin/rm /mnt/boot/*-fallback.img || true
