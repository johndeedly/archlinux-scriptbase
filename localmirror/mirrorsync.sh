#!/usr/bin/env bash

SYNC_HOME="$(dirname $(readlink -f "$0"))/localmirror"
SYNC_FILES="$SYNC_HOME/files"
SYNC_REPO=(core extra multilib iso)
SERVER_ARR="$(curl -S 'https://archlinux.org/mirrors/status/tier/1/json/' | jq -r '[.urls[] | select(.protocol == "rsync" and (.country_code == "AT" or .country_code == "BE" or .country_code == "DK" or .country_code == "FI" or .country_code == "FR" or .country_code == "DE" or .country_code == "IT" or .country_code == "NL" or .country_code == "NO" or .country_code == "PL" or .country_code == "ES" or .country_code == "SE" or .country_code == "CH" or .country_code == "GB") and .active)]')"
SERVER_ARR_LEN="$(echo -en $SERVER_ARR | jq '. | length')"
SERVER_SEL="$(shuf -i1-$SERVER_ARR_LEN -n1)"
SYNC_SERVER="$(echo -en $SERVER_ARR | jq -r ".[$SERVER_SEL].url")"

if [ ! -d "$SYNC_FILES" ]; then
  mkdir -p "$SYNC_FILES"
fi

echo ">> $SERVER_ARR_LEN tier 1 servers"
echo ">> choosing mirror $SYNC_SERVER"

for repo in ${SYNC_REPO[@]}; do
  repo=$(echo "$repo" | tr [:upper:] [:lower:])
  echo ">> Syncing $repo to $SYNC_FILES/$repo"
  rsync -rptlv --delete-after --safe-links --copy-links --delay-updates "$SYNC_SERVER/$repo" "$SYNC_FILES/"
  echo ">> Syncing $repo done."
  sleep 5
done
