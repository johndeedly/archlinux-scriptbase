#!/usr/bin/env bash

# configure error handling
set -e -o pipefail

# predefine functions
function installdeps() {
  local TMPPKG=$(mktemp)
  echo "install dependencies for $@"
  while (( "$#" )); do
    curl -sL "https://aur.archlinux.org/cgit/aur.git/plain/PKGBUILD?h=$1" > "${TMPPKG}"
    grep -e 'depends' "${TMPPKG}" && (
      unset depends
      unset makedepends
      unset checkdepends
      source "${TMPPKG}"
      ALLDEPENDS=("${depends[@]}" "${makedepends[@]}" "${checkdepends[@]}")
      for pkg in "${ALLDEPENDS[@]}"; do
        if [ ! -z "${pkg}" ]; then
          pacman -S --needed --noconfirm --asdeps --color=auto "${pkg}" || true
        fi
      done
    ) || echo 1>&2 "no dependencies parsed for $1"
    shift
  done
  /usr/bin/rm "${TMPPKG}"
}

# read administrative database to get usernames, group names and home directories
ROOTID=$(getent passwd 0 | cut -d: -f1)
ROOTGRP=$(getent group "$(getent passwd 0 | cut -d: -f4)" | cut -d: -f1)
ROOTHOME=$(getent passwd 0 | cut -d: -f6)
USERID=$(getent passwd 1000 | cut -d: -f1)
USERGRP=$(getent group "$(getent passwd 1000 | cut -d: -f4)" | cut -d: -f1)
USERHOME=$(getent passwd 1000 | cut -d: -f6)
TEMPID=$(getent passwd 4815 | cut -d: -f1)
TEMPGRP=$(getent group "$(getent passwd 4815 | cut -d: -f4)" | cut -d: -f1)
TEMPHOME=$(getent passwd 4815 | cut -d: -f6)
echo "admin: ${ROOTID}|${ROOTGRP}|${ROOTHOME}"
echo "user: ${USERID}|${USERGRP}|${USERHOME}"
echo "temp: ${TEMPID}|${TEMPGRP}|${TEMPHOME}"

# ILSpy
installdeps avaloniailspy
su -s /bin/bash - "${TEMPID}" <<EOS
git clone "https://aur.archlinux.org/avaloniailspy.git"
pushd avaloniailspy
  git config user.name ""
  git config user.email ""
  git submodule update --init --recursive || true
  makepkg --clean
popd
EOS
pacman -U --needed --noconfirm --color=auto "${TEMPHOME}"/avaloniailspy/avaloniailspy-*.pkg.tar.zst

# IDA free
installdeps ida-free
su -s /bin/bash - "${TEMPID}" <<EOS
git clone "https://aur.archlinux.org/ida-free.git"
pushd ida-free
  git config user.name ""
  git config user.email ""
  git submodule update --init --recursive || true
  makepkg --clean
popd
EOS
pacman -U --needed --noconfirm --color=auto "${TEMPHOME}"/ida-free/ida-free-*.pkg.tar.zst

# Redress
installdeps redress
su -s /bin/bash - "${TEMPID}" <<EOS
git clone "https://aur.archlinux.org/redress.git"
pushd redress
  git config user.name ""
  git config user.email ""
  git submodule update --init --recursive || true
  makepkg --clean
popd
EOS
pacman -U --needed --noconfirm --color=auto "${TEMPHOME}"/redress/redress-*.pkg.tar.zst

pacman -S --needed --noconfirm --color=auto rizin rz-ghidra rz-cutter python-rzpipe \
  ssdeep wireshark-qt \
  python-colorclass python-easygui python-pyparsing \
  torbrowser-launcher \
  libewf libguestfs veracrypt fuse2 btrfs-progs dosfstools exfatprogs \
  f2fs-tools e2fsprogs jfsutils nilfs-utils reiserfsprogs udftools \
  xfsprogs squashfs-tools nfs-utils yara \
  exploitdb binwalk diffoscope mitmproxy \
  thunderbird tor whois gnu-netcat websocat dbeaver \
  qt5-script

# xxd-standalone
installdeps xxd-standalone
su -s /bin/bash - "${TEMPID}" <<EOS
git clone "https://aur.archlinux.org/xxd-standalone.git"
pushd xxd-standalone
  git config user.name ""
  git config user.email ""
  git submodule update --init --recursive || true
  makepkg --clean
popd
EOS
pacman -U --needed --noconfirm --color=auto "${TEMPHOME}"/xxd-standalone/xxd-standalone-*.pkg.tar.zst

# python-msoffcrypto-tool
installdeps python-msoffcrypto-tool
su -s /bin/bash - "${TEMPID}" <<EOS
git clone "https://aur.archlinux.org/python-msoffcrypto-tool.git"
pushd python-msoffcrypto-tool
  git config user.name ""
  git config user.email ""
  git submodule update --init --recursive || true
  makepkg --clean
popd
EOS
pacman -U --needed --noconfirm --color=auto "${TEMPHOME}"/python-msoffcrypto-tool/python-msoffcrypto-tool-*.pkg.tar.zst

# python-oletools
installdeps python-oletools
su -s /bin/bash - "${TEMPID}" <<EOS
git clone "https://aur.archlinux.org/python-oletools.git"
pushd python-oletools
  git config user.name ""
  git config user.email ""
  git submodule update --init --recursive || true
  makepkg --clean
popd
EOS
pacman -U --needed --noconfirm --color=auto "${TEMPHOME}"/python-oletools/python-oletools-*.pkg.tar.zst

# ent
installdeps ent
su -s /bin/bash - "${TEMPID}" <<EOS
git clone "https://aur.archlinux.org/ent.git"
pushd ent
  git config user.name ""
  git config user.email ""
  git submodule update --init --recursive || true
  makepkg --clean
popd
EOS
pacman -U --needed --noconfirm --color=auto "${TEMPHOME}"/ent/ent-*.pkg.tar.zst

# detect-it-easy
installdeps detect-it-easy
su -s /bin/bash - "${TEMPID}" <<EOS
git clone "https://aur.archlinux.org/detect-it-easy.git"
pushd detect-it-easy
  git config user.name ""
  git config user.email ""
  git submodule update --init --recursive || true
  makepkg --clean
popd
EOS
pacman -U --needed --noconfirm --color=auto "${TEMPHOME}"/detect-it-easy/detect-it-easy-*.pkg.tar.zst

# rz_report
pacman -S --needed --noconfirm --asdeps --color=auto dotnet-runtime dotnet-sdk git
su -s /bin/bash - "${TEMPID}" <<EOS
git clone "https://gitlab.com/johndeedly/rz_report.git"
pushd rz_report
  git config user.name ""
  git config user.email ""
  git submodule update --init --recursive || true
  pushd pkgbuild
    makepkg --clean
  popd
popd
EOS
pacman -U --needed --noconfirm --color=auto "${TEMPHOME}"/rz_report/pkgbuild/rz_report-*.pkg.tar.zst

# install net framework 4.8 into wine
su -s /bin/bash - "${USERID}" <<EOS
pushd /tmp
  # uninstall mono
  wine uninstaller --remove '{E45D8920-A758-4088-B6C6-31DBB276992E}'
  # install net4.8
  winetricks -q dotnet48 || true
  # install vc runtime 2015-2019
  winetricks -q vcrun2019 || true
  # switch back to win7
  winetricks -q win7
popd
EOS

# dnSpyEx
su -s /bin/bash - "${USERID}" <<EOS
pushd /tmp
  if [[ -d dnSpyEx ]]; then
    /usr/bin/rm -r dnSpyEx
  fi
  mkdir dnSpyEx
  wget --server-response --timestamping https://github.com/dnSpyEx/dnSpy/releases/latest/download/dnSpy-net-win64.zip
  7z x -aoa -y -o"dnSpyEx" dnSpy-net-win64.zip
  /usr/bin/rm dnSpy-net-win64.zip
  mkdir -p "\$HOME/.local/opt"
  mv dnSpyEx "\$HOME/.local/opt/"
  chmod +x "\$HOME/.local/opt/dnSpyEx/dnSpy.exe"
  mkdir -p "\$HOME/.local/share/applications"
  tee "\$HOME/.local/share/applications/dnSpyEx.desktop" <<EOF
[Desktop Entry]
Type=Application
Name=dnSpyEx
Exec=\\\$HOME/.local/opt/dnSpyEx/dnSpy.exe
Path=\\\$HOME/.local/opt/dnSpyEx
Terminal=false
EOF
  # disable hw acceleration for wpf apps
  wine reg add "HKCU\\\\SOFTWARE\\\\Microsoft\\\\Avalon.Graphics" /v DisableHWAcceleration /t REG_DWORD /d 1 /f
popd
EOS

# bindiff
installdeps bindiff
su -s /bin/bash - "${TEMPID}" <<EOS
curl https://dl.google.com/linux/linux_signing_key.pub | gpg --import -
git clone "https://aur.archlinux.org/bindiff.git"
pushd bindiff
  git config user.name ""
  git config user.email ""
  git submodule update --init --recursive || true
  makepkg --clean
popd
EOS
pacman -U --needed --noconfirm --color=auto "${TEMPHOME}"/bindiff/bindiff-*.pkg.tar.zst

# extract-msg
su -s /bin/bash - "${USERID}" <<EOS
pip install extract-msg
EOS

# peepdf
su -s /bin/bash - "${USERID}" <<EOS
pip install peepdf
EOS

# python-evtx
su -s /bin/bash - "${USERID}" <<EOS
pip install python-evtx
EOS

# python-registry
su -s /bin/bash - "${USERID}" <<EOS
pip install python-registry
EOS

# x64dbg
su -s /bin/bash - "${USERID}" <<EOS
curl -sL 'https://api.github.com/repos/x64dbg/x64dbg/releases/latest' \\
| jq -r '.assets[0].browser_download_url' \\
| wget -O /tmp/x64dbg.zip -i -
7z x -o"\$HOME/.local/opt/x64dbg" /tmp/x64dbg.zip
rm /tmp/x64dbg.zip
tee \$HOME/.local/bin/x32dbg <<EOF
#!/usr/bin/env bash
cd \\\$HOME/.local/opt/x64dbg/release/x32 && wine32 x32dbg.exe \\\$@
EOF
tee \$HOME/.local/bin/x64dbg <<EOF
#!/usr/bin/env bash
cd \\\$HOME/.local/opt/x64dbg/release/x64 && wine x64dbg.exe \\\$@
EOF
chmod +x \$HOME/.local/bin/x32dbg \$HOME/.local/bin/x64dbg
tee \$HOME/.local/opt/x64dbg/x64dbg.reg <<EOF
Windows Registry Editor Version 5.00

[HKEY_CURRENT_USER\Software\Wine\AppDefaults]

[HKEY_CURRENT_USER\Software\Wine\AppDefaults\x32dbg.exe]

[HKEY_CURRENT_USER\Software\Wine\AppDefaults\x32dbg.exe\DllOverrides]
"dbghelp"="native,builtin"
"msvcp120"="native,builtin"
"msvcr120"="native,builtin"

[HKEY_CURRENT_USER\Software\Wine\AppDefaults\x64dbg.exe]

[HKEY_CURRENT_USER\Software\Wine\AppDefaults\x64dbg.exe\DllOverrides]
"dbghelp"="native,builtin"
"msvcp120"="native,builtin"
"msvcr120"="native,builtin"
EOF
wine regedit \$HOME/.local/opt/x64dbg/x64dbg.reg
WINEPREFIX="\$HOME/.local/wine32" WINEARCH=win32 wine regedit \$HOME/.local/opt/x64dbg/x64dbg.reg
mkdir -p ~/.local/share/applications
tee ~/.local/share/applications/x32dbg.desktop <<EOF
[Desktop Entry]
Type=Application
Name=x32dbg
Exec=x32dbg %f
TryExec=x32dbg
Terminal=false
Type=Application
EOF
tee ~/.local/share/applications/x64dbg.desktop <<EOF
[Desktop Entry]
Type=Application
Name=x64dbg
Exec=x64dbg %f
TryExec=x64dbg
Terminal=false
Type=Application
EOF
EOS

su -s /bin/bash - "${USERID}" <<EOS
pushd /tmp
  # posters
  if [[ -d posters ]]; then
    rm -r posters
  fi
  mkdir posters
  pushd posters
    webgrep -c -h - <<EOF
links = {}
-- get poster pages
if browser:NavigateTo('https://www.sans.org/posters/?focus-area=digital-forensics,penetration-testing-ethical-hacking') then
    onemore = true
    while onemore do
        carts = browser:AttachedElements('div.main-content li.article-listing__item')
        for elem in dotnet.each(carts) do
            title = elem:AttachedElements('div.title'):ToArray()[0]:GetText()
            link = elem:AttachedElements('a.block-link'):ToArray()[0]:GetAttribute('href')
            print(title .. ' - ' .. link)
            table.insert(links, { title, link })
        end
        next = browser:AttachedElements('div.pager li.page-next a'):ToArray()
        if next.Length == 0 then
            onemore = false
        else
            onemore = next[0]:ClickOn()
        end
    end
end

target = {}
-- get downloads for which no login is required
for k, v in pairs(links) do
    if browser:NavigateTo('https://www.sans.org' .. v[2]) then
        download = browser:AttachedElements('div.resource-header div.loginButton > a.button'):ToArray()
        if download.Length > 0 then
            v[2] = download[0]:GetAttribute('href'):gsub('/dl/', '/dd/')
            print('Found link for ' .. v[1] .. ": " .. v[2])
            table.insert(target, { v[1], v[2] })
        end
    end
end

-- down them all
for k, v in pairs(target) do
    print('Download ' .. v[1])
    filepath = browser:DownloadSaveToPath(nil, function()
        browser:NavigateToNoWait(v[2])
    end)
    os.rename(filepath, v[1] .. '.pdf')
end
EOF
  popd
  mv posters "${USERHOME}"/
  
  # papers
  if [[ -d papers ]]; then
    rm -r papers
  fi
  mkdir papers
  pushd papers
    # download some PDFs
    webgrep -c -h - <<EOF
filepath = browser:DownloadSaveToPath(nil, function()
    browser:NavigateToNoWait('https://www.blackhat.com/presentations/bh-dc-07/Sabanal_Yason/Paper/bh-dc-07-Sabanal_Yason-WP.pdf')
end)
print(filepath .. ' -> Reversing C++.pdf')
os.rename(filepath, 'Reversing C++.pdf')

filepath = browser:DownloadSaveToPath(nil, function()
    browser:NavigateToNoWait('http://users.ece.utexas.edu/~adnan/gdb-refcard.pdf')
end)
print(filepath .. ' -> Gnu Debug Reference (gdb).pdf')
os.rename(filepath, 'Gnu Debug Reference (gdb).pdf')

filepath = browser:DownloadSaveToPath(nil, function()
    browser:NavigateToNoWait('https://arxiv.org/pdf/2003.05039.pdf')
end)
print(filepath .. ' -> Devil is Virtual: Reversing Virtual Inheritance in C++ Binaries.pdf')
os.rename(filepath, 'Devil is Virtual: Reversing Virtual Inheritance in C++ Binaries.pdf')

filepath = browser:DownloadSaveToPath(nil, function()
    browser:NavigateToNoWait('https://hex-rays.com/tutorials/idaclang/idaclang_tutorial.pdf')
end)
print(filepath .. ' -> Using the IDAClang plugin for IDA Pro.pdf')
os.rename(filepath, 'Using the IDAClang plugin for IDA Pro.pdf')

filepath = browser:DownloadSaveToPath(nil, function()
    browser:NavigateToNoWait('https://recon.cx/2012/schedule/attachments/41_designing_mini_os_for_malware_analysis.pdf')
end)
print(filepath .. ' -> Designing mini os for malware analysis.pdf')
os.rename(filepath, 'Designing mini os for malware analysis.pdf')

filepath = browser:DownloadSaveToPath(nil, function()
    browser:NavigateToNoWait('https://hex-rays.com/wp-content/uploads/2019/12/debugging_appcall.pdf')
end)
print(filepath .. ' -> IDA Pro Appcall user guide.pdf')
os.rename(filepath, 'IDA Pro Appcall user guide.pdf')
EOF
    # convert some pages to PDFs
    webgrep -c -h - <<EOF
if browser:NavigateTo('https://www.binary-zone.com/2021/07/19/windows-kernel-debugging-using-two-vms-on-linux/') then
    browser:SaveScreenPdf('windows-kernel-debugging-using-two-vms-on-linux.pdf')
end
if browser:NavigateTo('http://ref.x86asm.net/coder32-abc.html') then
    browser:SaveScreenPdf('x86asm-coder32-abc.pdf')
end
if browser:NavigateTo('http://ref.x86asm.net/coder64-abc.html') then
    browser:SaveScreenPdf('x86asm-coder64-abc.pdf')
end
if browser:NavigateTo('http://polytechnitis.blogspot.com/2011/04/kernel-debugging-qemu-windows-vm-from.html') then
    browser:SaveScreenPdf('Kernel debugging a Qemu Windows VM from a Linux host.pdf')
end
EOF
    # convert CodeMachine articles to PDFs
    webgrep -c -h - <<EOF
browser:NavigateTo('https://codemachine.com/articles.html')
list = {}
cards = browser:AttachedElements('article div.card-body')
for card in dotnet.each(cards) do
    title = dotnet.first(card:AttachedElements('h2.card-title'))
    link = dotnet.first(card:AttachedElements('a.stretched-link'))
    if title ~= nil and link ~= nil then
        text = title:GetText()
        href = link:GetAttribute('href')
        table.insert(list, { text, href })
    end
end
page = browser:NewPage()
for key, value in ipairs(list) do
    print('CodeMachine article: ' .. value[1])
    if page:NavigateTo('https://codemachine.com' .. value[2], 90000) then
        page:Evaluate('() => document.getElementsByTagName("nav")[0].style.display = "none"')
        page:SaveScreenPdf('CodeMachine - ' .. value[1] .. '.pdf')
    end
end
EOF
  popd
  mv papers "${USERHOME}"/
popd
EOS

# loki
pip install yara-python psutil netaddr pylzma colorama rfc5424-logging-handler
su -s /bin/bash - "${USERID}" <<EOS
git clone --depth 1 https://github.com/Neo23x0/Loki.git "${USERHOME}"/.local/opt/loki || true
tee "${USERHOME}"/.local/bin/loki <<EOF
#!/usr/bin/env bash
curdir=\\\$(pwd)
cd \\\$HOME/.local/opt/loki && python loki.py --logfolder "\\\$curdir" \\\$@
EOF
chmod +x "${USERHOME}"/.local/bin/loki
chown -R "${USERID}:${USERGRP}" "${USERHOME}"/.local/opt/loki
pushd "${USERHOME}"/.local/opt/loki
  python loki-upgrader.py --sigsonly --debug || true
popd
EOS

# tor
sudo systemctl enable tor
sudo tee -a /usr/lib/firefox/firefox.cfg <<EOF
pref("network.proxy.socks", "localhost");
pref("network.proxy.socks_port", 9050);
pref("network.proxy.socks_version", 5);
pref("network.proxy.type", 1);
pref("network.proxy.socks_remote_dns", true);
pref("browser.startup.homepage", "https://check.torproject.org/");
EOF

# == cleanup everything ==
# - tmpusr installation data
if [ -d "${TEMPHOME}" ]; then
  /usr/bin/rm -r "${TEMPHOME}"/* || true
fi
# - package cache
echo -en "y\ny\n" | ( LC_ALL=C pacman -Scc )
# - logs
if [ -f /run/systemd/journal/io.systemd.journal ]; then
  journalctl --rotate
  journalctl --vacuum-time=1s
fi
fstrim --all
# - do not rebuild cache on boot
tee /etc/.updated /var/.updated <<EOF
# For details, see manpage of systemd.unit -> 'ConditionNeedsUpdate'.
TIMESTAMP_NSEC=$(date +%s%N)
EOF
chmod 644 /etc/.updated /var/.updated
