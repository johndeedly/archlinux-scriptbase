#!/usr/bin/env bash

# initialize root
USER="$(id -un)"
if [ ! "$EUID" -eq 0 ]; then
  sudo "$0" "$@"
  exit $?
else
  echo "Running as user '$USER'"
fi

# parse parameters
YES=""
POSITIONAL=()
while [ $# -gt 0 ]; do
  key="$1"
  case $key in
    -h|--help)
      shift
      echo "TODO"
      exit 1
      ;;
    -y|--yes)
      YES="YES"
      shift
      echo "[#] assume yes on every option"
      ;;
    *)
      POSITIONAL+=("$1") # save it in an array for later
      shift
      ;;
  esac
done
set -- "${POSITIONAL[@]}" # restore positional parameters

# integrity first
if ! mountpoint -q -- /mnt; then
  1>&2 echo "[!] /mnt doesn't appear to be mounted, aborting to prevent accidentally filling up the main filesystem"
  exit 1
elif [ ! -d /share ]; then
  1>&2 echo "[!] /share doesn't exist"
  exit 2
elif ! mountpoint -q -- /share; then
  1>&2 echo "[!] /share doesn't appear to be mounted, aborting to prevent accidentally filling up the main filesystem"
  exit 3
fi

tee /mnt/etc/.updated /mnt/var/.updated <<EOF
# For details, see manpage of systemd.unit -> 'ConditionNeedsUpdate'.
TIMESTAMP_NSEC=$(date +%s%N)
EOF
chmod 644 /mnt/etc/.updated /mnt/var/.updated

# http/nfs/nbd/cifs share
if [ ! -f /share/pxe/http_nfs_nbd_cifs/arch/x86_64/pxeboot.img ]; then
  mkdir -p /share/pxe/http_nfs_nbd_cifs/arch/x86_64
  findmnt -R /mnt
  mksquashfs /mnt /share/pxe/http_nfs_nbd_cifs/arch/x86_64/pxeboot.img -comp zstd -Xcompression-level 4 -b 1M -progress -wildcards \
    -e "boot/*" "dev/*" "etc/fstab" "etc/crypttab" "proc/*" "sys/*" "run/*" "mnt/*" "media/*" "tmp/*" "var/tmp/*" "var/cache/pacman/pkg/*"
fi

# # nbd share (old)
# if [ ! -f /share/pxe/nbd/arch.btrfs ]; then
#   mkdir -p /share/pxe/nbd
#   findmnt -R /mnt
#   truncate -s 24G /share/pxe/nbd/arch.btrfs
#   mkfs.btrfs /share/pxe/nbd/arch.btrfs
#   mount --mkdir -o loop,compress-force=zstd:4,flushoncommit,clear_cache /share/pxe/nbd/arch.btrfs /run/pxe/nbd
#   findmnt /run/pxe/nbd
#   mkdir -p /run/pxe/nbd/arch/x86_64
#   pv /share/pxe/http_nfs_cifs/arch/x86_64/pxeboot.img > /run/pxe/nbd/arch/x86_64/pxeboot.img
#   #rsync -aHAXS --info=progress2 --info=name0 --exclude={"boot/*","dev/*","etc/fstab","etc/crypttab","proc/*","sys/*","run/*","mnt/*","media/*","tmp/*","var/cache/pacman/pkg/*"} /mnt/ /tmp/pxe/nbd/
#   #sync
#   fuser -km /share/pxe/nbd/arch.btrfs || true
#   umount /run/pxe/nbd || true
# fi

# # tftp share
# if [ ! -f /share/pxe/tftp/pxelinux.cfg/default ]; then
#   mkdir -p /share/pxe/tftp/arch/x86_64 /share/pxe/tftp/pxelinux.cfg
#   cp -a /run/archiso/bootmnt/arch/boot/amd-ucode.img /share/pxe/tftp/arch/ || true
#   cp -a /run/archiso/bootmnt/arch/boot/intel-ucode.img /share/pxe/tftp/arch/ || true
#   cp -a /run/archiso/bootmnt/arch/boot/x86_64/vmlinuz-linux /share/pxe/tftp/arch/x86_64/ || true
#   cp -a /run/archiso/bootmnt/arch/boot/x86_64/initramfs-linux.img /share/pxe/tftp/arch/x86_64/ || true
#   cp -a /usr/lib/syslinux/bios/* /share/pxe/tftp/ || true
#   cp -a /usr/lib/syslinux/efi64/* /share/pxe/tftp/ || true
#   tee /share/pxe/tftp/pxelinux.cfg/default <<EOF
# DEFAULT arch
# LABEL arch
# LINUX arch/x86_64/vmlinuz-linux
# INITRD arch/intel-ucode.img,arch/amd-ucode.img,arch/x86_64/initramfs-linux.img
# APPEND archiso_nfs_srv=\${pxeserver}:/srv/nfs archisobasedir=arch copytoram=n cow_spacesize=80%
# IPAPPEND 3
# EOF
# fi
