#!/usr/bin/env bash

# configure error handling
set -e -o pipefail

# predefine functions
function installdeps() {
  local TMPPKG=$(mktemp)
  echo "install dependencies for $@"
  while (( "$#" )); do
    curl -sL "https://aur.archlinux.org/cgit/aur.git/plain/PKGBUILD?h=$1" > "${TMPPKG}"
    grep -e 'depends' "${TMPPKG}" && (
      unset depends
      unset makedepends
      unset checkdepends
      source "${TMPPKG}"
      ALLDEPENDS=("${depends[@]}" "${makedepends[@]}" "${checkdepends[@]}")
      for pkg in "${ALLDEPENDS[@]}"; do
        if [ ! -z "${pkg}" ]; then
          pacman -S --needed --noconfirm --asdeps --color=auto "${pkg}" || true
        fi
      done
    ) || echo 1>&2 "no dependencies parsed for $1"
    shift
  done
  /usr/bin/rm "${TMPPKG}"
}

# read administrative database to get usernames, group names and home directories
ROOTID=$(getent passwd 0 | cut -d: -f1)
ROOTGRP=$(getent group "$(getent passwd 0 | cut -d: -f4)" | cut -d: -f1)
ROOTHOME=$(getent passwd 0 | cut -d: -f6)
USERID=$(getent passwd 1000 | cut -d: -f1)
USERGRP=$(getent group "$(getent passwd 1000 | cut -d: -f4)" | cut -d: -f1)
USERHOME=$(getent passwd 1000 | cut -d: -f6)
TEMPID=$(getent passwd 4815 | cut -d: -f1)
TEMPGRP=$(getent group "$(getent passwd 4815 | cut -d: -f4)" | cut -d: -f1)
TEMPHOME=$(getent passwd 4815 | cut -d: -f6)
echo "admin: ${ROOTID}|${ROOTGRP}|${ROOTHOME}"
echo "user: ${USERID}|${USERGRP}|${USERHOME}"
echo "temp: ${TEMPID}|${TEMPGRP}|${TEMPHOME}"

# install cinnamon package
pacman -S --noconfirm --color=auto qtile python-dbus-next python-xlib pcmanfm-gtk3 lxsession-gtk3

# create skeleton for qtile
mkdir -p /etc/skel/.config/qtile "${ROOTHOME}"/.config/qtile "${USERHOME}"/.config/qtile
chown "${USERID}:${USERGRP}" "${USERHOME}"/.config/qtile
cp /root/qtile/qtileconfig.py /etc/skel/.config/qtile/config.py
cp /root/qtile/qtileconfig.py "${USERHOME}"/.config/qtile/config.py
chown "${USERID}:${USERGRP}" "${USERHOME}"/.config/qtile/config.py
cp /root/qtile/qtileconfig.py "${ROOTHOME}"/.config/qtile/config.py

# configure qtile to be the default window manager
sed -i 's/XDG_CURRENT_DESKTOP=.*/XDG_CURRENT_DESKTOP="qtile"/' /etc/skel/.xinitrc
tee -a /etc/skel/.xinitrc <<EOF

exec qtile start
EOF
cp /etc/skel/.xinitrc "${USERHOME}"/.xinitrc
cp /etc/skel/.xinitrc "${ROOTHOME}"/.xinitrc

# initiate a config reload on resolution change
tee -a /etc/skel/.xeventbind <<EOF

qtile cmd-obj -o cmd -f reload_config
EOF
cp /etc/skel/.xeventbind "${USERHOME}"/.xeventbind
cp /etc/skel/.xeventbind "${ROOTHOME}"/.xeventbind

# install qtile-extras
installdeps qtile-extras
su -s /bin/bash - "${TEMPID}" <<EOS
gpg --recv-keys "58A9AA7C86727DF7"
git clone "https://aur.archlinux.org/qtile-extras.git"
pushd qtile-extras
  git config user.name ""
  git config user.email ""
  git submodule update --init --recursive || true
  makepkg --clean
popd
EOS
pacman -U --needed --noconfirm --color=auto "${TEMPHOME}"/qtile-extras/qtile-extras-*.pkg.tar.zst

# create skeleton for pcmanfm
mkdir -p /etc/skel/.config/libfm "${ROOTHOME}"/.config/libfm "${USERHOME}"/.config/libfm
chown "${USERID}:${USERGRP}" "${USERHOME}"/.config/libfm
cp /root/qtile/libfm.conf /etc/skel/.config/libfm/libfm.conf
cp /root/qtile/libfm.conf "${USERHOME}"/.config/libfm/libfm.conf
chown "${USERID}:${USERGRP}" "${USERHOME}"/.config/libfm/libfm.conf
cp /root/qtile/libfm.conf "${ROOTHOME}"/.config/libfm/libfm.conf

# pcmanfm is default file manager
su - "${USERID}" <<EOS
# file explorer
[ -f /usr/share/applications/pcmanfm.desktop ] && \
  xdg-mime default pcmanfm.desktop `grep 'MimeType=' /usr/share/applications/pcmanfm.desktop | sed -e 's/.*=//' -e 's/;/ /g'`
EOS

# configure ly to have username and qtile prefilled on first boot
tee /etc/ly/save <<EOF
${USERID}
2
EOF

# == cleanup everything ==
# - tmpusr installation data
if [ -d "${TEMPHOME}" ]; then
  /usr/bin/rm -r "${TEMPHOME}"/* || true
fi
# - package cache
echo -en "y\ny\n" | ( LC_ALL=C pacman -Scc )
# - logs
if [ -f /run/systemd/journal/io.systemd.journal ]; then
  journalctl --rotate
  journalctl --vacuum-time=1s
fi
fstrim --all
# - do not rebuild cache on boot
tee /etc/.updated /var/.updated <<EOF
# For details, see manpage of systemd.unit -> 'ConditionNeedsUpdate'.
TIMESTAMP_NSEC=$(date +%s%N)
EOF
chmod 644 /etc/.updated /var/.updated
