#!/usr/bin/env bash

# initialize root
USER="$(id -un)"
if [ ! "$EUID" -eq 0 ]; then
  sudo "$0" "$@"
  exit $?
else
  echo "Running as user '$USER'"
fi

# parse parameters
DEVICE=""
ENCRYPTION=""
DUALBOOT=""
YES=""
POSITIONAL=()
while [ $# -gt 0 ]; do
  key="$1"
  case $key in
    -h|--help)
      shift
      echo "TODO"
      exit 1
      ;;
    -d|--device)
      DEVICE="$2"
      shift
      shift
      echo "[#] using DEVICE=${DEVICE}"
      ;;
    -e|--encryption)
      ENCRYPTION="$2"
      shift
      shift
      echo "[#] using ENCRYPTION=${ENCRYPTION}"
      ;;
    -b|--dualboot)
      DUALBOOT="YES"
      shift
      echo "[#] dualboot partition setup"
      ;;
    -y|--yes)
      YES="YES"
      shift
      echo "[#] assume yes on every option"
      ;;
    *)
      POSITIONAL+=("$1") # save it in an array for later
      shift
      ;;
  esac
done
set -- "${POSITIONAL[@]}" # restore positional parameters

# resize tmpfs
mount -o remount,size=80% /run/archiso/cowspace

# read system setup
readarray -t devicelist < <(lsblk -dplnx size -o name,size | grep -Ev "boot|rpmb|loop" | tac)
if [ -z "${DEVICE}" ]; then
  echo "Select installation disk:"
  select opt in "${devicelist[@]}"
  do
    if ! [ -z "$opt" ]; then
      DEVICE=$(echo "$opt" | cut -d' ' -f1)
      break
    fi
  done
fi

part_efi="$(ls ${DEVICE}* | grep -E "^${DEVICE}p?1$")"
if [ -z "${DUALBOOT}" ]; then
  part_root="$(ls ${DEVICE}* | grep -E "^${DEVICE}p?2$")"
  part_data="$(ls ${DEVICE}* | grep -E "^${DEVICE}p?3$")"
else
  part_msr="$(ls ${DEVICE}* | grep -E "^${DEVICE}p?2$")"
  part_win="$(ls ${DEVICE}* | grep -E "^${DEVICE}p?3$")"
  part_diag="$(ls ${DEVICE}* | grep -E "^${DEVICE}p?4$")"
  part_root="$(ls ${DEVICE}* | grep -E "^${DEVICE}p?5$")"
  part_windata="$(ls ${DEVICE}* | grep -E "^${DEVICE}p?6$")"
  part_data="$(ls ${DEVICE}* | grep -E "^${DEVICE}p?7$")"
fi
data_id="data"
volgrp_id="volgrp"

# ask for the encryption password if "YES" is not set
if [ -z "${ENCRYPTION}" ]; then
  if [ -z "${YES}" ]; then
    echo "Enter password for encrypted ${data_id} partition"
    ENCRYPTION=$(ask_passwd "(leave empty for no encryption)")
  fi
fi

if ! [ -z "$ENCRYPTION" ]; then
  echo -n "${ENCRYPTION}" | (cryptsetup -q open ${part_data} ${data_id} -d -)
fi

/usr/bin/lvm vgchange -aay --autoactivation event volgrp

# mount everything in place
mount -o subvol=@ ${part_root} /mnt
#mount -o subvol=@.snapshots ${part_root} /mnt/.snapshots
mount ${part_efi} /mnt/boot

#mount -o subvol=@,compress=zstd:4,noatime /dev/${volgrp_id}/${data_id} /mnt/data
mount -o subvol=@home,compress=zstd:4,noatime /dev/${volgrp_id}/${data_id} /mnt/home
mount -o subvol=@var,compress=zstd:4,noatime /dev/${volgrp_id}/${data_id} /mnt/var
mount -o subvol=@root,compress=zstd:4,noatime /dev/${volgrp_id}/${data_id} /mnt/root
mount -o subvol=@srv,compress=zstd:4,noatime /dev/${volgrp_id}/${data_id} /mnt/srv
mount -o subvol=@opt,compress=zstd:4,noatime /dev/${volgrp_id}/${data_id} /mnt/opt
#mount -o subvol=@.snapshots /dev/${volgrp_id}/${data_id} /mnt/data/.snapshots

# enabling ufw filters inside container
modprobe iptable_filter
modprobe ip6table_filter
