#!/usr/bin/env bash

# initialize root
USER="$(id -un)"
if [ ! "$EUID" -eq 0 ]; then
  sudo "$0" "$@"
  exit $?
else
  echo "Running as user '$USER'"
fi

# parse parameters
DEVICE=""
ENCRYPTION=""
DUALBOOT=""
YES=""
POSITIONAL=()
while [ $# -gt 0 ]; do
  key="$1"
  case $key in
    -h|--help)
      shift
      echo "TODO"
      exit 1
      ;;
    -d|--device)
      DEVICE="$2"
      shift
      shift
      echo "[#] using DEVICE=${DEVICE}"
      ;;
    -b|--dualboot)
      DUALBOOT="YES"
      shift
      echo "[#] dualboot partition setup"
      ;;
    -y|--yes)
      YES="YES"
      shift
      echo "[#] assume yes on every option"
      ;;
    *)
      POSITIONAL+=("$1") # save it in an array for later
      shift
      ;;
  esac
done
set -- "${POSITIONAL[@]}" # restore positional parameters

# read system setup
readarray -t devicelist < <(lsblk -dplnx size -o name,size | grep -Ev "boot|rpmb|loop" | tac)
if [ -z "${DEVICE}" ]; then
  echo "Select installation disk:"
  select opt in "${devicelist[@]}"
  do
    if ! [ -z "$opt" ]; then
      DEVICE=$(echo "$opt" | cut -d' ' -f1)
      break
    fi
  done
fi

part_efi="$(ls ${DEVICE}* | grep -E "^${DEVICE}p?1$")"
if [ -z "${DUALBOOT}" ]; then
  part_root="$(ls ${DEVICE}* | grep -E "^${DEVICE}p?2$")"
  part_data="$(ls ${DEVICE}* | grep -E "^${DEVICE}p?3$")"
else
  part_msr="$(ls ${DEVICE}* | grep -E "^${DEVICE}p?2$")"
  part_win="$(ls ${DEVICE}* | grep -E "^${DEVICE}p?3$")"
  part_diag="$(ls ${DEVICE}* | grep -E "^${DEVICE}p?4$")"
  part_root="$(ls ${DEVICE}* | grep -E "^${DEVICE}p?5$")"
  part_windata="$(ls ${DEVICE}* | grep -E "^${DEVICE}p?6$")"
  part_data="$(ls ${DEVICE}* | grep -E "^${DEVICE}p?7$")"
fi
data_id="data"
volgrp_id="volgrp"

# finishing steps
arch-chroot /mnt hwclock --systohc --utc || true

# enable additional services
arch-chroot /mnt systemctl enable systemd-timesyncd fstrim.timer

# build btrfs-autosnap
arch-chroot /mnt pacman -S --needed --noconfirm --asdeps --color=auto btrfs-progs coreutils util-linux
arch-chroot /mnt su -s /bin/bash - tmpusr <<EOS
git clone "https://aur.archlinux.org/btrfs-autosnap.git"
pushd btrfs-autosnap
  git config user.name ""
  git config user.email ""
  git submodule update --init --recursive || true
  makepkg --clean
popd
EOS
arch-chroot /mnt /bin/bash -c 'pacman -U --needed --noconfirm --color=auto /home/tmpusr/btrfs-autosnap/btrfs-autosnap-*.pkg.tar.zst'
sed -i '0,/^#\?old=.*/s//old="4"/' /mnt/etc/btrfs-autosnap.conf
sed -i '0,/^#\?subvolume=.*/s//subvolume="@"/' /mnt/etc/btrfs-autosnap.conf
# disabled until the user really wants to enable it by removing the /dev/null softlink
mkdir -p /mnt/etc/pacman.d/hooks
ln -s /dev/null /mnt/etc/pacman.d/hooks/01-btrfs-autosnap.hook

# networkd-dispatcher
arch-chroot /mnt pacman -S --needed --noconfirm --asdeps --color=auto dbus-glib python dbus-python python-gobject asciidoc iw
arch-chroot /mnt su -s /bin/bash - tmpusr <<EOS
git clone "https://aur.archlinux.org/networkd-dispatcher.git"
pushd networkd-dispatcher
  git config user.name ""
  git config user.email ""
  git submodule update --init --recursive || true
  makepkg --clean
popd
EOS
arch-chroot /mnt /bin/bash -c 'pacman -U --needed --noconfirm --color=auto /home/tmpusr/networkd-dispatcher/networkd-dispatcher-*.pkg.tar.zst'

tee /mnt/etc/networkd-dispatcher/routable.d/00-logger <<EOF
#! /bin/bash -e

LOG () { echo \${*:- } ;} >&2

LOG IFACE=\$IFACE \$OperationalState
EOF
chmod +x /mnt/etc/networkd-dispatcher/routable.d/00-logger
ln -s /etc/networkd-dispatcher/routable.d/00-logger /mnt/etc/networkd-dispatcher/carrier.d/00-logger
ln -s /etc/networkd-dispatcher/routable.d/00-logger /mnt/etc/networkd-dispatcher/configured.d/00-logger
ln -s /etc/networkd-dispatcher/routable.d/00-logger /mnt/etc/networkd-dispatcher/configuring.d/00-logger
ln -s /etc/networkd-dispatcher/routable.d/00-logger /mnt/etc/networkd-dispatcher/degraded.d/00-logger
ln -s /etc/networkd-dispatcher/routable.d/00-logger /mnt/etc/networkd-dispatcher/dormant.d/00-logger
ln -s /etc/networkd-dispatcher/routable.d/00-logger /mnt/etc/networkd-dispatcher/no-carrier.d/00-logger
ln -s /etc/networkd-dispatcher/routable.d/00-logger /mnt/etc/networkd-dispatcher/off.d/00-logger

tee /mnt/etc/networkd-dispatcher/routable.d/20-wired <<EOF
#! /bin/bash -e

LOG () { echo \${*:- } ;} >&2

case \$IFACE in
  "en"*)
    if systemd-analyze verify iwd.service 2>/dev/null; then
      LOG IFACE=\$IFACE stop iwd service
      systemctl stop iwd
    fi
  ;;
esac
EOF
chmod +x /mnt/etc/networkd-dispatcher/routable.d/20-wired

tee /mnt/etc/networkd-dispatcher/off.d/20-wired <<EOF
#! /bin/bash -e

LOG () { echo \${*:- } ;} >&2

case \$IFACE in
  "en"*)
    if systemd-analyze verify iwd.service 2>/dev/null; then
      LOG IFACE=\$IFACE start iwd service
      systemctl start iwd
    fi
  ;;
esac
EOF
chmod +x /mnt/etc/networkd-dispatcher/off.d/20-wired
ln -s /etc/networkd-dispatcher/off.d/20-wired /mnt/etc/networkd-dispatcher/no-carrier.d/20-wired

mkdir /mnt/etc/systemd/system/networkd-dispatcher.service.d
tee /mnt/etc/systemd/system/networkd-dispatcher.service.d/override.conf <<EOF
[Unit]
Wants=iwd.service
After=iwd.service
After=systemd-udev-settle.service
After=systemd-networkd.service
EOF
arch-chroot /mnt systemctl enable networkd-dispatcher

# list all hardware
LSHARDWARE=$( arch-chroot /mnt lshw -xml -quiet )

# detect wireless device
WIRELESS=$(echo ${LSHARDWARE} | xmllint --xpath 'count(//capability[@id="wireless"])' -)
if [[ ${WIRELESS} -gt 0 ]]; then
  arch-chroot /mnt pacman -S --needed --noconfirm --color=auto iwd iw
  arch-chroot /mnt systemctl enable iwd
  # ipv6 enable and 
  tee /mnt/etc/iwd/main.conf <<EOF
[General]
EnableNetworkConfiguration=false

[Network]
EnableIPv6=true
NameResolvingService=systemd
EOF
  # enable systemd-networkd device naming
  ln -s /dev/null /mnt/etc/systemd/network/80-iwd.link
fi

# detect bluetooth device
BLUETOOTH=$(echo ${LSHARDWARE} | xmllint --xpath 'count(//capability[@id="bluetooth"])' -)
if [[ ${BLUETOOTH} -gt 0 ]]; then
  arch-chroot /mnt pacman -S --needed --noconfirm --color=auto bluez bluez-utils bluez-plugins
  arch-chroot /mnt systemctl enable bluetooth
  # enable bluetooth autoenable of devices
  sed -i 's/^#\?AutoEnable=.*/AutoEnable=true/' /mnt/etc/bluetooth/main.conf
fi

# disable system beep
rmmod pcspkr || true
echo "blacklist pcspkr" | tee /mnt/etc/modprobe.d/nobeep.conf

# detect installed cpu and install appropriate microcodes
CPU=$(echo ${LSHARDWARE} | xmllint --xpath '//node[@class="processor"]/vendor/text()' -)
if [[ $CPU =~ [Aa][Mm][Dd] ]]; then
  echo "AMD cpu detected."
  arch-chroot /mnt pacman -S --needed --noconfirm --color=auto amd-ucode
elif [[ $CPU =~ [Ii][Nn][Tt][Ee][Ll] ]]; then
  echo "Intel cpu detected."
  arch-chroot /mnt pacman -S --needed --noconfirm --color=auto intel-ucode
fi

# detect installed gpus -> multiple gpus means multiple drivers installed
GPUS=$(echo ${LSHARDWARE} | xmllint --xpath '//node[@class="display"]/vendor/text()' -)
for elem in $GPUS; do
  if [[ $elem =~ [Aa][Mm][Dd] || $elem =~ [Aa][Tt][Ii] ]]; then
    echo "AMD/ATI gpu detected."
    arch-chroot /mnt pacman -S --needed --noconfirm --color=auto xf86-video-ati xf86-video-amdgpu mesa vulkan-radeon libva-mesa-driver mesa-vdpau libva-vdpau-driver libva-utils radeontop
    # fix screen tearing
    # https://linuxreviews.org/HOWTO_fix_screen_tearing
    mkdir -p /mnt/etc/X11/xorg.conf.d
    tee /mnt/etc/X11/xorg.conf.d/20-amdgpu.conf <<EOF
Section "Device"
    Identifier "AMD Graphics"
    Driver "amdgpu"
    Option "TearFree" "true"
EndSection
EOF
    tee /mnt/etc/X11/xorg.conf.d/20-radeon.conf <<EOF
Section "Device"
    Identifier "AMD Graphics"
    Driver "radeon"
    Option "TearFree" "true"
EndSection
EOF
  elif [[ $elem =~ [Nn][Vv][Ii][Dd][Ii][Aa] ]]; then
    echo "NVIDIA gpu detected."
    arch-chroot /mnt pacman -S --needed --noconfirm --color=auto xf86-video-nouveau mesa libva-mesa-driver mesa-vdpau libva-utils nvidia-smi
    # fix screen tearing (hopefully, maybe?)
    mkdir -p /mnt/etc/X11/xorg.conf.d
    tee /mnt/etc/X11/xorg.conf.d/20-nouveau.conf <<EOF
Section "Device"
    Identifier "NVIDIA Graphics"
    Driver "nouveau"
    Option "TearFree" "true"
EndSection
EOF
  elif [[ $elem =~ [Ii][Nn][Tt][Ee][Ll] ]]; then
    echo "Intel gpu detected."
    arch-chroot /mnt pacman -S --needed --noconfirm --color=auto xf86-video-intel mesa vulkan-intel libva-mesa-driver mesa-vdpau libva-intel-driver libva-utils intel-gpu-tools
    # fix screen tearing
    # https://linuxreviews.org/HOWTO_fix_screen_tearing
    mkdir -p /mnt/etc/X11/xorg.conf.d
    tee /mnt/etc/X11/xorg.conf.d/20-intel-gpu.conf <<EOF
Section "Device"
    Identifier "Intel Graphics"
    Driver "intel"
    Option "TearFree" "true"
EndSection
EOF
  elif [[ $elem =~ [Ii][Nn][Nn][Oo][Tt][Ee][Kk] || $elem =~ [Vv][Mm][Ww][Aa][Rr][Ee] || $elem =~ [Vv][Ii][Rr][Tt][Uu][Aa][Ll][Bb][Oo][Xx] || $elem =~ [Vv][Bb][Oo][Xx] ]]; then
    echo "Virtual gpu detected."
    arch-chroot /mnt pacman -S --needed --noconfirm --color=auto amd-ucode intel-ucode
    arch-chroot /mnt pacman -S --needed --noconfirm --color=auto xf86-video-vmware mesa libva-mesa-driver mesa-vdpau libva-utils virtualbox-guest-utils
    arch-chroot /mnt systemctl enable vboxservice
    tee /mnt/usr/bin/userlogin.sh <<EOF
setsid -f /usr/bin/VBoxClient-all
EOF
    # fix screen tearing (hopefully, maybe?)
    mkdir -p /mnt/etc/X11/xorg.conf.d
    tee /mnt/etc/X11/xorg.conf.d/20-vmware.conf <<EOF
Section "Device"
    Identifier "VMWARE Graphics"
    Driver "vmware"
    Option "TearFree" "true"
EndSection
EOF
  fi
done

# enable user services
tee /mnt/etc/systemd/system/suspend@.service <<EOF
[Unit]
Description=User suspend actions
Before=sleep.target

[Service]
User=%I
Type=forking
Environment=DISPLAY=:0
ExecStart=/usr/bin/xautolock -locknow
ExecStartPost=/usr/bin/sleep 1

[Install]
WantedBy=sleep.target
EOF
tee /mnt/etc/systemd/system/resume@.service <<EOF
[Unit]
Description=User resume actions
After=suspend.target hibernate.target hybrid-sleep.target suspend-then-hibernate.target

[Service]
User=root
Type=oneshot
ExecStartPre=/usr/bin/sleep 2
ExecStart=/usr/bin/systemctl restart bluetooth iwd networkd-dispatcher
TimeoutSec=0

[Install]
WantedBy=suspend.target hibernate.target hybrid-sleep.target suspend-then-hibernate.target
EOF
arch-chroot /mnt systemctl enable suspend@"$(</root/bootstrap/user.usr)".service
arch-chroot /mnt systemctl enable resume@"$(</root/bootstrap/user.usr)".service

# when in a virtual environment configure the user to be added to vboxsf group to enable file shares
for elem in $GPUS; do
  if [[ $elem =~ [Ii][Nn][Nn][Oo][Tt][Ee][Kk] || $elem =~ [Vv][Mm][Ww][Aa][Rr][Ee] ]]; then
    echo "Virtual gpu detected."
    arch-chroot /mnt usermod -a -G vboxsf "$(</root/bootstrap/user.usr)"
  fi
done

# create boot image and loader
if [ -f /mnt/boot/luks.key ]; then
  sed -i "s/^MODULES=(.*/MODULES=(vfat)/g" /mnt/etc/mkinitcpio.conf
  sed -i "s/^HOOKS=(.*/HOOKS=(base systemd autodetect keyboard sd-vconsole modconf block lvm2 sd-encrypt filesystems resume fsck)/g" /mnt/etc/mkinitcpio.conf
else
  sed -i "s/^HOOKS=(.*/HOOKS=(base systemd autodetect keyboard sd-vconsole modconf block lvm2 filesystems resume fsck)/g" /mnt/etc/mkinitcpio.conf
fi
arch-chroot /mnt mkinitcpio -P

arch-chroot /mnt bootctl install

root_uuid=$(blkid ${part_root} -s UUID -o value)
if [ -f /mnt/boot/luks.key ]; then
  crypt_uuid=$(cryptsetup luksUUID ${part_data})
  bootoptions="options root=UUID=${root_uuid} rootflags=subvol=@ rw luks.name=${crypt_uuid}=${data_id} luks.key=${crypt_uuid}=/luks.key:LABEL=EFI loglevel=3 acpi_osi=Linux resume=/dev/mapper/${volgrp_id}-swap"
else
  bootoptions="options root=UUID=${root_uuid} rootflags=subvol=@ rw loglevel=3 acpi_osi=Linux resume=/dev/mapper/${volgrp_id}-swap"
fi
if [ -f /mnt/boot/amd-ucode.img ]; then
  amdinitrdopt="initrd  /amd-ucode.img"
else
  amdinitrdopt=""
fi
if [ -f /mnt/boot/intel-ucode.img ]; then
  intelinitrdopt="initrd  /intel-ucode.img"
else
  intelinitrdopt=""
fi

tee /mnt/boot/loader/entries/80_arch.conf <<EOF
title   Arch Linux
linux   /vmlinuz-linux
$amdinitrdopt
$intelinitrdopt
initrd  /initramfs-linux.img
$bootoptions
EOF

tee /mnt/boot/loader/entries/60_arch_lts.conf <<EOF
title   Arch Linux (LTS kernel)
linux   /vmlinuz-linux-lts
$amdinitrdopt
$intelinitrdopt
initrd  /initramfs-linux-lts.img
$bootoptions
EOF

tee /mnt/boot/loader/entries/40_windows.conf <<EOF
title   Windows Boot Manager
efi     /EFI/Microsoft/Boot/bootmgfw.efi
EOF

tee /mnt/boot/loader/loader.conf <<EOF
default  @saved
timeout  4
editor   no
auto-entries   no
auto-firmware  yes
console-mode   max
EOF

arch-chroot /mnt bootctl update

# == cleanup everything ==
# - tmpusr installation data
if [ -d /mnt/home/tmpusr ]; then
  /usr/bin/rm -r /mnt/home/tmpusr/* || true
fi
# - package cache
echo -en "y\ny\n" | ( arch-chroot /mnt bash -c "LC_ALL=C pacman -Scc" )
# - logs
arch-chroot /mnt journalctl --rotate
arch-chroot /mnt journalctl --vacuum-time=1s
arch-chroot /mnt fstrim --all
# - do not rebuild cache on boot
tee /mnt/etc/.updated /mnt/var/.updated <<EOF
# For details, see manpage of systemd.unit -> 'ConditionNeedsUpdate'.
TIMESTAMP_NSEC=$(date +%s%N)
EOF
chmod 644 /mnt/etc/.updated /mnt/var/.updated
