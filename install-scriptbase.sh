#!/usr/bin/env bash

# initialize root
USER="$(id -un)"
if [ ! "$EUID" -eq 0 ]; then
  sudo "$0" "$@"
  exit $?
else
  echo "Running as user '$USER'"
fi

# parse parameters
WORKDIR=""
STAGES=()
POSITIONAL=()
while [ $# -gt 0 ]; do
  key="$1"
  case $key in
    -h|--help)
      shift
      echo "TODO"
      exit 1
      ;;
    -w|--workdir)
      WORKDIR="$2"
      shift
      shift
      echo "[#] using WORKDIR=${WORKDIR}"
      ;;
    -s|--stage)
      STAGES+=("$2")
      shift
      shift
      echo "[#] using STAGES=${STAGES[@]}"
      ;;
    *)
      POSITIONAL+=("$1") # save it in an array for later
      shift
      ;;
  esac
done
set -- "${POSITIONAL[@]}" # restore positional parameters

# workdir is always the current working directory as a fallback
if [ -z "${WORKDIR}" ]; then
  WORKDIR="$(pwd)"
  echo "[#] using WORKDIR=${WORKDIR}"
fi

for STAGE in "${STAGES[@]}"
do
  if ! [ -d "${WORKDIR}/${STAGE}" ]; then
    1>&2 echo "[!] stage folder ${WORKDIR}/${STAGE} not found"
    exit 1
  elif ! [ -f "${WORKDIR}/${STAGE}/configure-${STAGE}.sh" ]; then
    1>&2 echo "[!] configure script ${WORKDIR}/${STAGE}/configure-${STAGE}.sh not found"
    exit 2
  elif ! mountpoint -q -- /mnt; then
    1>&2 echo "[!] /mnt doesn't appear to be mounted, aborting to prevent accidentally filling up the main filesystem"
    exit 3
  fi

  mount --mkdir -t tmpfs -o size=100M none "/mnt/root/${STAGE}"
  cp -r "${WORKDIR}/${STAGE}"/* "/mnt/root/${STAGE}/"
  chmod +x "/mnt/root/${STAGE}/configure-${STAGE}.sh"

  systemd-nspawn --capability=CAP_SYS_CHROOT,CAP_NET_ADMIN,CAP_NET_RAW --pipe -D /mnt <<EOS
pushd "/root/${STAGE}"
  /usr/bin/bash "configure-${STAGE}.sh" || tee last_error_code <<EOF
\$?
EOF
popd
logout
EOS

  if [ -f "/mnt/root/${STAGE}/last_error_code" ]; then
    last_error_code="$( head -n 1 "/mnt/root/${STAGE}/last_error_code" )"
    echo "Script /mnt/root/${STAGE}/configure-${STAGE}.sh exited with error code ${last_error_code}"
    exit "${last_error_code}"
  fi

  umount "/mnt/root/${STAGE}"
  rm -r "/mnt/root/${STAGE}" || true
done
