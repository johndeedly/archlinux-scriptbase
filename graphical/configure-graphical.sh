#!/usr/bin/env bash

# configure error handling
set -e -o pipefail

# predefine functions
function installdeps() {
  local SEDFILE=()
  local TMPARR=()
  while [[ $# -gt 0 ]]; do
    key="$1"
    case $key in
      -s|--sed)
        SEDFILE+=("$2")
        shift
        shift
        ;;
      *)
        TMPARR+=("$1") # save it in an array for later
        shift # past argument
        ;;
    esac
  done
  set -- "${TMPARR[@]}"

  local TMPPKG=$(mktemp)
  echo "install dependencies for $@"
  while (( "$#" )); do
    curl -sL "https://aur.archlinux.org/cgit/aur.git/plain/PKGBUILD?h=$1" > "${TMPPKG}"
    grep -e 'depends' "${TMPPKG}" && (
      unset depends
      unset makedepends
      unset checkdepends
      for repl in "${SEDFILE[@]}"; do
        if [ ! -z "${repl}" ]; then
          sed -i -e "${repl}" "${TMPPKG}"
        fi
      done
      source "${TMPPKG}"
      ALLDEPENDS=("${depends[@]}" "${makedepends[@]}" "${checkdepends[@]}")
      for pkg in "${ALLDEPENDS[@]}"; do
        if [ ! -z "${pkg}" ]; then
          pacman -S --needed --noconfirm --asdeps --color=auto "${pkg}" || true
        fi
      done
    ) || echo 1>&2 "no dependencies parsed for $1"
    shift
  done
  /usr/bin/rm "${TMPPKG}"
}

# read administrative database to get usernames, group names and home directories
ROOTID=$(getent passwd 0 | cut -d: -f1)
ROOTGRP=$(getent group "$(getent passwd 0 | cut -d: -f4)" | cut -d: -f1)
ROOTHOME=$(getent passwd 0 | cut -d: -f6)
USERID=$(getent passwd 1000 | cut -d: -f1)
USERGRP=$(getent group "$(getent passwd 1000 | cut -d: -f4)" | cut -d: -f1)
USERHOME=$(getent passwd 1000 | cut -d: -f6)
TEMPID=$(getent passwd 4815 | cut -d: -f1)
TEMPGRP=$(getent group "$(getent passwd 4815 | cut -d: -f4)" | cut -d: -f1)
TEMPHOME=$(getent passwd 4815 | cut -d: -f6)
echo "admin: ${ROOTID}|${ROOTGRP}|${ROOTHOME}"
echo "user: ${USERID}|${USERGRP}|${USERHOME}"
echo "temp: ${TEMPID}|${TEMPGRP}|${TEMPHOME}"

# sound
pacman -S --needed --noconfirm --color=auto pipewire pipewire-pulse pipewire-jack pipewire-alsa wireplumber \
  pamixer pavucontrol playerctl alsa-utils qpwgraph
# xorg server with some essential programs and libraries
pacman -S --needed --noconfirm --color=auto xorg-server xorg-xinit xorg-xrandr xautolock xclip xsel \
  brightnessctl gammastep arandr dunst libnotify xarchiver \
  flameshot libinput xf86-input-libinput xorg-xinput kitty wofi dex
# wallpapers, fonts and icons
pacman -S --needed --noconfirm --color=auto archlinux-wallpaper elementary-wallpapers papirus-icon-theme \
  ttf-liberation ttf-font-awesome ttf-hanazono ttf-hannom \
  ttf-baekmuk noto-fonts-emoji
# complementary programs
pacman -S --needed --noconfirm --color=auto libreoffice-fresh libreoffice-fresh-de krita evolution \
  gnome-keyring freerdp notepadqq gitg firefox chromium keepassxc pdfpc zettlr obsidian texlive-bin \
  xdg-desktop-portal xdg-desktop-portal-gtk wine winetricks mpv gpicview qalculate-gtk

# slock
installdeps slock-git
su -s /bin/bash - "${TEMPID}" <<EOS
git clone "https://aur.archlinux.org/slock-git.git"
pushd slock-git
  git config user.name ""
  git config user.email ""
  git submodule update --init --recursive || true
  sed -i 's/sed.*-i config.def.h/sed -i -e '"'"'\/group =\/s\/nogroup\/nobody\/'"'"' -e '"'"'s\/#CC3333\/#661919\/'"'"' config.def.h/' PKGBUILD
  makepkg --clean
popd
EOS
pacman -U --needed --noconfirm --color=auto "${TEMPHOME}"/slock-git/slock-git-*.pkg.tar.zst

# build pamac
installdeps -s 's/^ENABLE_FLATPAK.*/ENABLE_FLATPAK=1/' -s 's/^ENABLE_SNAPD.*/ENABLE_SNAPD=0/' libpamac-aur
su -s /bin/bash - "${TEMPID}" <<EOS
git clone "https://aur.archlinux.org/libpamac-aur.git"
pushd libpamac-aur
  git config user.name ""
  git config user.email ""
  git submodule update --init --recursive || true
  sed -i -e 's/^ENABLE_FLATPAK.*/ENABLE_FLATPAK=1/' -e 's/^ENABLE_SNAPD.*/ENABLE_SNAPD=0/' PKGBUILD
  makepkg --clean
popd
EOS
pacman -U --needed --noconfirm --color=auto "${TEMPHOME}"/libpamac-aur/libpamac-aur-*.pkg.tar.zst

installdeps pamac-aur
su -s /bin/bash - "${TEMPID}" <<EOS
git clone "https://aur.archlinux.org/pamac-aur.git"
pushd pamac-aur
  git config user.name ""
  git config user.email ""
  git submodule update --init --recursive || true
  makepkg --clean
popd
EOS
pacman -U --needed --noconfirm --color=auto "${TEMPHOME}"/pamac-aur/pamac-aur-*.pkg.tar.zst

# drawio
installdeps drawio-desktop-bin
su -s /bin/bash - "${TEMPID}" <<EOS
git clone "https://aur.archlinux.org/drawio-desktop-bin.git"
pushd drawio-desktop-bin
  git config user.name ""
  git config user.email ""
  git submodule update --init --recursive || true
  makepkg --clean
popd
EOS
pacman -U --needed --noconfirm --color=auto "${TEMPHOME}"/drawio-desktop-bin/drawio-desktop-bin-*.pkg.tar.zst

## camunda modeler
#installdeps camunda-modeler
#su -s /bin/bash - "${TEMPID}" <<EOS
#git clone "https://aur.archlinux.org/camunda-modeler.git"
#pushd camunda-modeler
#  git config user.name ""
#  git config user.email ""
#  git submodule update --init --recursive || true
#  makepkg --clean
#popd
#EOS
#pacman -U --needed --noconfirm --color=auto "${TEMPHOME}"/camunda-modeler/camunda-modeler-*.pkg.tar.zst

## autotiling
#installdeps autotiling
#su -s /bin/bash - "${TEMPID}" <<EOS
#git clone "https://aur.archlinux.org/autotiling.git"
#pushd autotiling
#  git config user.name ""
#  git config user.email ""
#  git submodule update --init --recursive || true
#  makepkg --clean
#popd
#EOS
#pacman -U --needed --noconfirm --color=auto "${TEMPHOME}"/autotiling/autotiling-*.pkg.tar.zst

## sysinfo
#su -s /bin/bash - "${TEMPID}" <<EOS
#git clone https://gitlab.com/johndeedly/sysinfo.git
#pushd sysinfo
#  git config user.name ""
#  git config user.email ""
#  git submodule update --init --recursive || true
#  pushd pkgbuild
#    makepkg --clean
#  popd
#popd
#EOS
#pacman -U --needed --noconfirm --color=auto "${TEMPHOME}"/sysinfo/pkgbuild/sysinfo-*.pkg.tar.zst

# corner coasting still not supported in libinput
# https://gitlab.freedesktop.org/libinput/libinput/-/issues/536
if ! [ -d /usr/share/X11/xorg.conf.d ]; then
  mkdir -p /usr/share/X11/xorg.conf.d
fi
cp /root/graphical/30-touchpad.conf /usr/share/X11/xorg.conf.d/30-touchpad.conf

# configure firefox
update-ca-trust
# https[://]support[.]mozilla[.]org/en-US/kb/customizing-firefox-using-autoconfig
mkdir -p /usr/lib/firefox/defaults/pref
cp /root/graphical/autoconfig.js /usr/lib/firefox/defaults/pref/autoconfig.js
chmod 0644 /usr/lib/firefox/defaults/pref/autoconfig.js
# https[://]github[.]com/arkenfox/user.js/blob/master/user.js
cp /root/graphical/firefox.cfg /usr/lib/firefox/firefox.cfg
chmod 0644 /usr/lib/firefox/firefox.cfg
# https[://]github[.]com/serv-inc/safe-search/wiki/Bulletproof-installation
mkdir -p /usr/lib/firefox/distribution
cp /root/graphical/policies.json /usr/lib/firefox/distribution/policies.json
chmod 0644 /usr/lib/firefox/distribution/policies.json

# https://github.com/stove-panini/fontconfig-emoji
# https://aur.archlinux.org/cgit/aur.git/tree/75-twemoji.conf?h=ttf-twemoji
cp /root/graphical/69-emoji.conf /etc/fonts/conf.d/69-emoji.conf
cp /root/graphical/70-no-mozilla-emoji.conf /etc/fonts/conf.d/70-no-mozilla-emoji.conf
cp /root/graphical/70-no-fa-regular.conf /etc/fonts/conf.d/70-no-fa-regular.conf
fc-cache -f -v

# set regionals for x11
cp /root/graphical/00-keyboard.conf /etc/X11/xorg.conf.d/00-keyboard.conf

# enable software cursor
# https://superuser.com/q/1546702
cp /root/graphical/swcursor.conf /usr/share/X11/xorg.conf.d/05-swcursor.conf

# create skeleton for dmrc
cp /root/graphical/dmrc /etc/skel/.dmrc
cp /root/graphical/dmrc "${USERHOME}"/.dmrc
chown "${USERID}:${USERGRP}" "${USERHOME}"/.dmrc
cp /root/graphical/dmrc "${ROOTHOME}"/.dmrc

# create skeleton for dunstrc
mkdir -p /etc/skel/.config/dunst "${ROOTHOME}"/.config/dunst "${USERHOME}"/.config/dunst
chown "${USERID}:${USERGRP}" "${USERHOME}"/.config/dunst
cp /root/graphical/dunstrc /etc/skel/.config/dunst/dunstrc
cp /root/graphical/dunstrc "${USERHOME}"/.config/dunst/dunstrc
chown "${USERID}:${USERGRP}" "${USERHOME}"/.config/dunst/dunstrc
cp /root/graphical/dunstrc "${ROOTHOME}"/.config/dunst/dunstrc

# create skeleton for fontconfig
mkdir -p /etc/skel/.config/fontconfig "${ROOTHOME}"/.config/fontconfig "${USERHOME}"/.config/fontconfig
chown "${USERID}:${USERGRP}" "${USERHOME}"/.config/fontconfig
cp /root/graphical/fonts.conf /etc/skel/.config/fontconfig/fonts.conf
cp /root/graphical/fonts.conf "${USERHOME}"/.config/fontconfig/fonts.conf
chown "${USERID}:${USERGRP}" "${USERHOME}"/.config/fontconfig/fonts.conf
cp /root/graphical/fonts.conf "${ROOTHOME}"/.config/fontconfig/fonts.conf

# create skeleton for gtkrc-2.0
cp /root/graphical/gtkrc-2.0 /etc/skel/.gtkrc-2.0
cp /root/graphical/gtkrc-2.0 "${USERHOME}"/.gtkrc-2.0
chown "${USERID}:${USERGRP}" "${USERHOME}"/.gtkrc-2.0
cp /root/graphical/gtkrc-2.0 "${ROOTHOME}"/.gtkrc-2.0

# create skeleton for gtk-3.0
mkdir -p /etc/skel/.config/gtk-3.0 "${ROOTHOME}"/.config/gtk-3.0 "${USERHOME}"/.config/gtk-3.0
chown "${USERID}:${USERGRP}" "${USERHOME}"/.config/gtk-3.0
cp /root/graphical/gtk-3.0 /etc/skel/.config/gtk-3.0/settings.ini
cp /root/graphical/gtk-3.0 "${USERHOME}"/.config/gtk-3.0/settings.ini
chown "${USERID}:${USERGRP}" "${USERHOME}"/.config/gtk-3.0/settings.ini
cp /root/graphical/gtk-3.0 "${ROOTHOME}"/.config/gtk-3.0/settings.ini

# create skeleton for gtk-4.0
mkdir -p /etc/skel/.config/gtk-4.0 "${ROOTHOME}"/.config/gtk-4.0 "${USERHOME}"/.config/gtk-4.0
chown "${USERID}:${USERGRP}" "${USERHOME}"/.config/gtk-4.0
cp /root/graphical/gtk-4.0 /etc/skel/.config/gtk-4.0/settings.ini
cp /root/graphical/gtk-4.0 "${USERHOME}"/.config/gtk-4.0/settings.ini
chown "${USERID}:${USERGRP}" "${USERHOME}"/.config/gtk-4.0/settings.ini
cp /root/graphical/gtk-4.0 "${ROOTHOME}"/.config/gtk-4.0/settings.ini

# create skeleton for gammastep
mkdir -p /etc/skel/.config/gammastep/hooks "${ROOTHOME}"/.config/gammastep/hooks "${USERHOME}"/.config/gammastep/hooks
cp /root/graphical/darkmode.sh /etc/skel/.config/gammastep/hooks/darkmode
chmod +x /etc/skel/.config/gammastep/hooks/darkmode
cp /root/graphical/darkmode.sh "${USERHOME}"/.config/gammastep/hooks/darkmode
chmod +x "${USERHOME}"/.config/gammastep/hooks/darkmode
cp /root/graphical/darkmode.sh "${ROOTHOME}"/.config/gammastep/hooks/darkmode
chmod +x "${ROOTHOME}"/.config/gammastep/hooks/darkmode
chown -R "${USERID}:${USERGRP}" "${USERHOME}/.config/gammastep"

# create skeleton for slock_run
mkdir -p /etc/skel/.local/bin "${ROOTHOME}"/.local/bin "${USERHOME}"/.local/bin
chown "${USERID}:${USERGRP}" "${USERHOME}"/.local/bin
cp /root/graphical/slock_run /etc/skel/.local/bin/slock_run
chmod +x /etc/skel/.local/bin/slock_run
cp /root/graphical/slock_run "${USERHOME}"/.local/bin/slock_run
chown "${USERID}:${USERGRP}" "${USERHOME}"/.local/bin/slock_run
chmod +x "${USERHOME}"/.local/bin/slock_run
cp /root/graphical/slock_run "${ROOTHOME}"/.local/bin/slock_run
chmod +x "${ROOTHOME}"/.local/bin/slock_run

# create skeleton for i3
#mkdir -p /etc/skel/.config/i3 "${ROOTHOME}"/.config/i3 "${USERHOME}"/.config/i3
#chown "${USERID}:${USERGRP}" "${USERHOME}"/.config/i3
#cp /root/graphical/i3config /etc/skel/.config/i3/config
#cp /root/graphical/i3config "${USERHOME}"/.config/i3/config
#chown "${USERID}:${USERGRP}" "${USERHOME}"/.config/i3/config
#cp /root/graphical/i3config "${ROOTHOME}"/.config/i3/config

# create skeleton for wofi
mkdir -p /etc/skel/.config/wofi "${ROOTHOME}"/.config/wofi "${USERHOME}"/.config/wofi
chown "${USERID}:${USERGRP}" "${USERHOME}"/.config/wofi
cp /root/graphical/style.css /etc/skel/.config/wofi/style.css
cp /root/graphical/style.css "${USERHOME}"/.config/wofi/style.css
chown "${USERID}:${USERGRP}" "${USERHOME}"/.config/wofi/style.css
cp /root/graphical/style.css "${ROOTHOME}"/.config/wofi/style.css

# create skeleton for xarchiverrc
mkdir -p /etc/skel/.config/xarchiver "${ROOTHOME}"/.config/xarchiver "${USERHOME}"/.config/xarchiver
chown "${USERID}:${USERGRP}" "${USERHOME}"/.config/xarchiver
cp /root/graphical/xarchiverrc /etc/skel/.config/xarchiver/xarchiverrc
cp /root/graphical/xarchiverrc "${USERHOME}"/.config/xarchiver/xarchiverrc
chown "${USERID}:${USERGRP}" "${USERHOME}"/.config/xarchiver/xarchiverrc
cp /root/graphical/xarchiverrc "${ROOTHOME}"/.config/xarchiver/xarchiverrc

# create skeleton for Xdefaults
cp /root/graphical/Xdefaults /etc/skel/.Xdefaults
cp /root/graphical/Xdefaults "${USERHOME}"/.Xdefaults
chown "${USERID}:${USERGRP}" "${USERHOME}"/.Xdefaults
cp /root/graphical/Xdefaults "${ROOTHOME}"/.Xdefaults

# create skeleton for xinitrc
cp /root/graphical/xinitrc /etc/skel/.xinitrc
chmod +x /etc/skel/.xinitrc
cp /root/graphical/xinitrc "${USERHOME}"/.xinitrc
chown "${USERID}:${USERGRP}" "${USERHOME}"/.xinitrc
chmod +x "${USERHOME}"/.xinitrc
cp /root/graphical/xinitrc "${ROOTHOME}"/.xinitrc
chmod +x "${ROOTHOME}"/.xinitrc

# create skeleton for Xmodmap
cp /root/graphical/Xmodmap /etc/skel/.Xmodmap
cp /root/graphical/Xmodmap "${USERHOME}"/.Xmodmap
chown "${USERID}:${USERGRP}" "${USERHOME}"/.Xmodmap
cp /root/graphical/Xmodmap "${ROOTHOME}"/.Xmodmap

# create skeleton for Xresources
cp /root/graphical/Xresources /etc/skel/.Xresources
cp /root/graphical/Xresources "${USERHOME}"/.Xresources
chown "${USERID}:${USERGRP}" "${USERHOME}"/.Xresources
cp /root/graphical/Xresources "${ROOTHOME}"/.Xresources

# create skeleton for chromium
# Force Dark Mode for Web Contents - https://superuser.com/a/1642278
mkdir -p /etc/skel/.config "${ROOTHOME}"/.config "${USERHOME}"/.config
chown "${USERID}:${USERGRP}" "${USERHOME}"/.config
cp /root/graphical/chromium-flags.conf /etc/skel/.config/chromium-flags.conf
cp /root/graphical/chromium-flags.conf "${USERHOME}"/.config/chromium-flags.conf
chown "${USERID}:${USERGRP}" "${USERHOME}"/.config/chromium-flags.conf
cp /root/graphical/chromium-flags.conf "${ROOTHOME}"/.config/chromium-flags.conf

# configure chromium policies
mkdir -p /etc/chromium/policies/managed
cp /root/graphical/adblock.json /etc/chromium/policies/managed/adblock.json
cp /root/graphical/default-settings.json /etc/chromium/policies/managed/default-settings.json
cp /root/graphical/extensions-default.json /etc/chromium/policies/managed/extensions-default.json
cp /root/graphical/telemetry-off.json /etc/chromium/policies/managed/telemetry-off.json
cp /root/graphical/duckduckgo.json /etc/chromium/policies/managed/duckduckgo.json
cp /root/graphical/restore-session.json /etc/chromium/policies/managed/restore-session.json

# global xterm fallback to kitty
ln -s /usr/bin/kitty /usr/bin/xterm

# wine first time startup
su - "${USERID}" <<EOS
wine wineboot -u
tee "${USERHOME}/.local/bin/wine32" <<EOF
#!/usr/bin/env bash

WINEPREFIX="\\\$HOME/.local/wine32" WINEARCH=win32 wine \\\$@
EOF
chmod +x "${USERHOME}/.local/bin/wine32"
WINEPREFIX="${USERHOME}/.local/wine32" WINEARCH=win32 wine wineboot -u
EOS

# set xdg-mime defaults
su - "${USERID}" <<EOS
# libreoffice
[ -f /usr/share/applications/libreoffice-math.desktop ] && \
  xdg-mime default libreoffice-math.desktop `grep 'MimeType=' /usr/share/applications/libreoffice-math.desktop | sed -e 's/.*=//' -e 's/;/ /g'`
[ -f /usr/share/applications/libreoffice-draw.desktop ] && \
  xdg-mime default libreoffice-draw.desktop `grep 'MimeType=' /usr/share/applications/libreoffice-draw.desktop | sed -e 's/.*=//' -e 's/;/ /g'`
[ -f /usr/share/applications/libreoffice-calc.desktop ] && \
  xdg-mime default libreoffice-calc.desktop `grep 'MimeType=' /usr/share/applications/libreoffice-calc.desktop | sed -e 's/.*=//' -e 's/;/ /g'`
[ -f /usr/share/applications/libreoffice-writer.desktop ] && \
  xdg-mime default libreoffice-writer.desktop `grep 'MimeType=' /usr/share/applications/libreoffice-writer.desktop | sed -e 's/.*=//' -e 's/;/ /g'`
[ -f /usr/share/applications/libreoffice-impress.desktop ] && \
  xdg-mime default libreoffice-impress.desktop `grep 'MimeType=' /usr/share/applications/libreoffice-impress.desktop | sed -e 's/.*=//' -e 's/;/ /g'`

# browser (libreoffice first, then firefox)
[ -f /usr/share/applications/firefox.desktop ] && \
  xdg-mime default firefox.desktop `grep 'MimeType=' /usr/share/applications/firefox.desktop | sed -e 's/.*=//' -e 's/;/ /g'`

# email program
[ -f /usr/share/applications/org.gnome.Evolution.desktop ] && \
  xdg-mime default org.gnome.Evolution.desktop `grep 'MimeType=' /usr/share/applications/org.gnome.Evolution.desktop | sed -e 's/.*=//' -e 's/;/ /g'`

# archive program
[ -f /usr/share/applications/xarchiver.desktop ] && \
  xdg-mime default xarchiver.desktop `grep 'MimeType=' /usr/share/applications/xarchiver.desktop | sed -e 's/.*=//' -e 's/;/ /g'`

# video playback program
[ -f /usr/share/applications/mpv.desktop ] && \
  xdg-mime default mpv.desktop `grep 'MimeType=' /usr/share/applications/mpv.desktop | sed -e 's/.*=//' -e 's/;/ /g'`

# notepad program
[ -f /usr/share/applications/notepadqq.desktop ] && \
  xdg-mime default notepadqq.desktop `grep 'MimeType=' /usr/share/applications/notepadqq.desktop | sed -e 's/.*=//' -e 's/;/ /g'`

# image previewer (firefox first, then gpicview)
[ -f /usr/share/applications/gpicview.desktop ] && \
  xdg-mime default gpicview.desktop `grep 'MimeType=' /usr/share/applications/gpicview.desktop | sed -e 's/.*=//' -e 's/;/ /g'`
EOS

# set xdg-settings default applications
su - "${USERID}" <<EOS
xdg-settings set default-web-browser firefox.desktop
xdg-settings set default-url-scheme-handler mailto org.gnome.Evolution.desktop
xdg-mime default firefox.desktop x-scheme-handler/https x-scheme-handler/http
EOS

# install some fonts (#1)
installdeps ttf-dejavu-emojiless
su -s /bin/bash - "${TEMPID}" <<EOS
git clone "https://aur.archlinux.org/ttf-dejavu-emojiless.git"
pushd ttf-dejavu-emojiless
  git config user.name ""
  git config user.email ""
  git submodule update --init --recursive || true
  makepkg --clean
popd
EOS
pacman -U --needed --noconfirm --color=auto "${TEMPHOME}"/ttf-dejavu-emojiless/ttf-dejavu-emojiless-*.pkg.tar.zst

# install some fonts (#2)
installdeps ttf-ms-fonts
su -s /bin/bash - "${TEMPID}" <<EOS
git clone "https://aur.archlinux.org/ttf-ms-fonts.git"
pushd ttf-ms-fonts
  git config user.name ""
  git config user.email ""
  git submodule update --init --recursive || true
  makepkg --clean
popd
EOS
pacman -U --needed --noconfirm --color=auto "${TEMPHOME}"/ttf-ms-fonts/ttf-ms-fonts-*.pkg.tar.zst

# enable rdp
installdeps xrdp
su -s /bin/bash - "${TEMPID}" <<EOS
git clone "https://aur.archlinux.org/xrdp.git"
pushd xrdp
  git config user.name ""
  git config user.email ""
  git submodule update --init --recursive || true
  makepkg --clean
popd
EOS
pacman -U --needed --noconfirm --color=auto "${TEMPHOME}"/xrdp/xrdp-*.pkg.tar.zst
systemctl enable xrdp xrdp-sesman
ufw disable
ufw limit 3389/tcp comment 'allow limited xrdp access'
ufw enable
sed -i 's/^# exec xterm/exec \/bin\/bash --login -i ~\/.xinitrc/' /etc/xrdp/startwm.sh

# # configure firefox addons
# su -s /bin/bash - "${TEMPID}" <<EOS
# mkdir adguard && cd adguard
# tee PKGBUILD <<EOF
# pkgname=firefox-adguard
# pkgver=3.5.31
# pkgrel=1
# pkgdesc="Unmatched adblock extension against advertising and pop-ups. Blocks ads on Facebook, Youtube and all other websites."
# arch=('i686' 'x86_64')
# url=https://adguard.com
# license=('LGPLv3')
# source=("https://addons.mozilla.org/firefox/downloads/file/3696663/adguard_adblocker-\\\${pkgver}-an+fx.xpi")
# noextract=("\\\${source##*/}")
# sha256sums=('6ddaf4da351a6aa29b062857295eb17d200e707ffba7fb88e7c2df4568442f92')
# 
# prepare() {
#   mkdir tmp && cd tmp
#   bsdtar -xf "../\\\${source##*/}"
#   sed -i 's/\[ "storage" \]/\[ "storage" \],\
#     "browser_specific_settings": {\
#       "gecko": {\
#         "id": "adguardadblocker@adguard.com"\
#       }\
#     }/' manifest.json
#   bsdtar -caf addon.zip *
# }
# 
# package() {
#   install -Dm644 tmp/addon.zip "\\\$pkgdir"/usr/lib/firefox/browser/extensions/adguardadblocker@adguard.com.xpi
# }
# EOF
# makepkg --clean
# EOS
# pacman -U --needed --noconfirm --color=auto "${TEMPHOME}"/adguard/firefox-adguard-*-x86_64.pkg.tar.zst
# 
# su -s /bin/bash - "${TEMPID}" <<EOS
# mkdir forgetmenot && cd forgetmenot
# tee PKGBUILD <<EOF
# pkgname=firefox-forgetmenot
# pkgver=2.2.8
# pkgrel=1
# pkgdesc="Make the browser forget website data (like cookies, local storage, etc.), except for the data you want to keep by adding domains to a whitelist, graylist, blacklist, or redlist."
# arch=('i686' 'x86_64')
# url=https://github.com/Lusito/forget-me-not/
# license=('zlib')
# source=("https://addons.mozilla.org/firefox/downloads/file/3577046/forget_me_not_forget_cookies_other_data-\\\${pkgver}-an+fx.xpi")
# noextract=("\\\${source##*/}")
# sha256sums=('0784456f4f992c143b7897ea7879ce6324d9cce295c29a848e7ed55d9c762be3')
# 
# prepare() {
#   mkdir tmp && cd tmp
#   bsdtar -xf "../\\\${source##*/}"
#   sed -i 's/\[ "storage" \]/\[ "storage" \],\
#     "browser_specific_settings": {\
#       "gecko": {\
#         "id": "forget-me-not@lusito.info"\
#       }\
#     }/' manifest.json
#   bsdtar -caf addon.zip *
# }
# 
# package() {
#   install -Dm644 tmp/addon.zip "\\\$pkgdir"/usr/lib/firefox/browser/extensions/forget-me-not@lusito.info.xpi
# }
# EOF
# makepkg --clean
# EOS
# pacman -U --needed --noconfirm --color=auto "${TEMPHOME}"/forgetmenot/firefox-forgetmenot-*-x86_64.pkg.tar.zst
# 
# su -s /bin/bash - "${TEMPID}" <<EOS
# mkdir keepassxc && cd keepassxc
# tee PKGBUILD <<EOF
# pkgname=firefox-keepassxc
# pkgver=1.7.8.1
# pkgrel=1
# pkgdesc="Official browser plugin for the KeePassXC password manager."
# arch=('i686' 'x86_64')
# url=https://keepassxc.org/
# license=('AGPLv3')
# source=("https://addons.mozilla.org/firefox/downloads/file/3758952/keepassxc_browser-\\\${pkgver}-fx.xpi")
# noextract=("\\\${source##*/}")
# sha256sums=('c091084b5ac5acbf4652bd60033a69e10d1b1e3e5ff3dd1f68fc62afea636b3d')
# 
# prepare() {
#   mkdir tmp && cd tmp
#   bsdtar -xf "../\\\${source##*/}"
#   sed -i 's/\[ "storage" \]/\[ "storage" \],\
#     "browser_specific_settings": {\
#       "gecko": {\
#         "id": "keepassxc-browser@keepassxc.org"\
#       }\
#     }/' manifest.json
#   bsdtar -caf addon.zip *
# }
# 
# package() {
#   install -Dm644 tmp/addon.zip "\\\$pkgdir"/usr/lib/firefox/browser/extensions/keepassxc-browser@keepassxc.org.xpi
# }
# EOF
# makepkg --clean
# EOS
# pacman -U --needed --noconfirm --color=auto "${TEMPHOME}"/keepassxc/firefox-keepassxc-*-x86_64.pkg.tar.zst
# 
# su -s /bin/bash - "${TEMPID}" <<EOS
# mkdir singlefile && cd singlefile
# tee PKGBUILD <<EOF
# pkgname=firefox-singlefile
# pkgver=1.18.66
# pkgrel=1
# pkgdesc="Save an entire web page—including images and styling—as a single HTML file."
# arch=('i686' 'x86_64')
# url=https://github.com/gildas-lormeau/SingleFile
# license=('AGPLv3')
# source=("https://addons.mozilla.org/firefox/downloads/file/3715227/singlefile-\\\${pkgver}-an+fx.xpi")
# noextract=("\\\${source##*/}")
# sha256sums=('7edaa0e21deb4a69b4eb5e6a810322a74fa76fcff991cfce24f03c293101bde9')
# 
# prepare() {
#   mkdir tmp && cd tmp
#   bsdtar -xf "../\\\${source##*/}"
#   sed -i 's/\[ "storage" \]/\[ "storage" \],\
#     "browser_specific_settings": {\
#       "gecko": {\
#         "id": "{531906d3-e22f-4a6c-a102-8057b88a1a63}"\
#       }\
#     }/' manifest.json
#   bsdtar -caf addon.zip *
# }
# 
# package() {
#   install -Dm644 tmp/addon.zip "\\\$pkgdir"/usr/lib/firefox/browser/extensions/{531906d3-e22f-4a6c-a102-8057b88a1a63}.xpi
# }
# EOF
# makepkg --clean
# EOS
# pacman -U --needed --noconfirm --color=auto "${TEMPHOME}"/singlefile/firefox-singlefile-*-x86_64.pkg.tar.zst
# 
# su -s /bin/bash - "${TEMPID}" <<EOS
# mkdir sponsorblock && cd sponsorblock
# tee PKGBUILD <<EOF
# pkgname=firefox-sponsorblock
# pkgver=3.6.1
# pkgrel=1
# pkgdesc="Easily skip YouTube video sponsors. When you visit a YouTube video, the extension will check the database for reported sponsors and automatically skip known sponsors. You can also report sponsors in videos."
# arch=('i686' 'x86_64')
# url=https://sponsor.ajay.app/
# license=('LGPLv3')
# source=("https://addons.mozilla.org/firefox/downloads/file/3866051/sponsorblock_skip_sponsorships_on_youtube-\\\${pkgver}-an+fx.xpi")
# noextract=("\\\${source##*/}")
# sha256sums=('d426be3a22e93fd55156fadcc7a6fac83237d74f103abb32b7111984d7db0f40')
# 
# prepare() {
#   mkdir tmp && cd tmp
#   bsdtar -xf "../\\\${source##*/}"
#   sed -i 's/\[ "storage" \]/\[ "storage" \],\
#     "browser_specific_settings": {\
#       "gecko": {\
#         "id": "sponsorBlocker@ajay.app"\
#       }\
#     }/' manifest.json
#   bsdtar -caf addon.zip *
# }
# 
# package() {
#   install -Dm644 tmp/addon.zip "\\\$pkgdir"/usr/lib/firefox/browser/extensions/sponsorBlocker@ajay.app.xpi
# }
# EOF
# makepkg --clean
# EOS
# pacman -U --needed --noconfirm --color=auto "${TEMPHOME}"/sponsorblock/firefox-sponsorblock-*-x86_64.pkg.tar.zst

# install webgrep
pacman -S --needed --noconfirm --asdeps --color=auto alsa-lib dbus-glib
su -s /bin/bash - "${TEMPID}" <<EOS
git clone https://gitlab.com/johndeedly/webgrep.git
pushd webgrep
  git config user.name ""
  git config user.email ""
  git submodule update --init --recursive || true
  shopt -s globstar
  for x in **/*.csproj; do
    sed -i 's/net7.0/net8.0/g' "\$x"
  done
  pushd pkgbuild
    makepkg --clean
  popd
popd
EOS
pacman -U --needed --noconfirm --color=auto "${TEMPHOME}"/webgrep/pkgbuild/webgrep-*.pkg.tar.zst
su -s /bin/bash - "${USERID}" <<EOS
webgrep -h -i firefox -i chromium - <<EOF
EOF
EOS
find "${USERHOME}"/.cache/ms-playwright -type f -name 'libstdc++.so.6' | while read libstd; do
    echo "fixing ${libstd} by linking it to /usr/lib64/libstdc++.so.6"
    rm "${libstd}"
    ln -s /usr/lib64/libstdc++.so.6 "${libstd}"
    chown "${USERID}:${USERGRP}" "${libstd}"
done
mkdir -p "${ROOTHOME}"/.cache/ms-playwright
cp -r "${USERHOME}"/.cache/ms-playwright/* "${ROOTHOME}"/.cache/ms-playwright/

# xeventbind
su -s /bin/bash - "${TEMPID}" <<EOS
git clone "https://github.com/ritave/xeventbind.git"
pushd xeventbind
  git config user.name ""
  git config user.email ""
  git submodule update --init --recursive || true
  make
popd
EOS
cp "${TEMPHOME}"/xeventbind/xeventbind /usr/bin/xeventbind
chmod 0755 /usr/bin/xeventbind
mkdir -p /usr/share/licenses/xeventbind
cp "${TEMPHOME}"/xeventbind/LICENSE /usr/share/licenses/xeventbind/LICENSE
chmod 0644 /usr/share/licenses/xeventbind/LICENSE

# create skeleton for xeventbind
cp /root/graphical/xeventbind /etc/skel/.xeventbind
chmod +x /etc/skel/.xeventbind
cp /root/graphical/xeventbind "${USERHOME}"/.xeventbind
chown "${USERID}:${USERGRP}" "${USERHOME}"/.xeventbind
chmod +x "${USERHOME}"/.xeventbind
cp /root/graphical/xeventbind "${ROOTHOME}"/.xeventbind
chmod +x "${ROOTHOME}"/.xeventbind

# TODO: some installation step overwrites the time settings in /etc/localtime to UTC, need to investigate
# restore i18n
sed -i 's/^#\?de_DE.UTF-8 UTF-8/de_DE.UTF-8 UTF-8/' /etc/locale.gen
locale-gen
echo "LANG=de_DE.UTF-8" > /etc/locale.conf
rm /etc/localtime || true
ln -s /usr/share/zoneinfo/CET /etc/localtime
loadkeys de || true
tee /etc/vconsole.conf <<EOF
KEYMAP=de
XKBLAYOUT=de
XKBMODEL=pc105
EOF

# == cleanup everything ==
# - tmpusr installation data
if [ -d "${TEMPHOME}" ]; then
  /usr/bin/rm -r "${TEMPHOME}"/* || true
fi
# - package cache
echo -en "y\ny\n" | ( LC_ALL=C pacman -Scc )
# - logs
if [ -f /run/systemd/journal/io.systemd.journal ]; then
  journalctl --rotate
  journalctl --vacuum-time=1s
fi
fstrim --all
# - do not rebuild cache on boot
tee /etc/.updated /var/.updated <<EOF
# For details, see manpage of systemd.unit -> 'ConditionNeedsUpdate'.
TIMESTAMP_NSEC=$(date +%s%N)
EOF
chmod 644 /etc/.updated /var/.updated
