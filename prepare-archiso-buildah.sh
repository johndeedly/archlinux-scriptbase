#!/usr/bin/env bash

# initialize root
USER="$(id -un)"
if [ ! "$EUID" -eq 0 ]; then
  sudo "$0" "$@"
  exit $?
else
  echo "Running as user '$USER'"
fi

# parse parameters
DEVICE=""
ENCRYPTION=""
YES=""
POSITIONAL=()
while [ $# -gt 0 ]; do
  key="$1"
  case $key in
    -h|--help)
      shift
      echo "TODO"
      exit 1
      ;;
    -d|--device)
      DEVICE="$2"
      shift
      shift
      echo "[#] using DEVICE=${DEVICE}"
      ;;
    -e|--encryption)
      ENCRYPTION="$2"
      shift
      shift
      echo "[#] using ENCRYPTION=${ENCRYPTION}"
      ;;
    -y|--yes)
      YES="YES"
      shift
      echo "[#] assume yes on every option"
      ;;
    *)
      POSITIONAL+=("$1") # save it in an array for later
      shift
      ;;
  esac
done
set -- "${POSITIONAL[@]}" # restore positional parameters

# resize tmpfs
mount -o remount,size=80% /run/archiso/cowspace

# read system setup
readarray -t devicelist < <(lsblk -dplnx size -o name,size | grep -Ev "boot|rpmb|loop" | tac)
if [ -z "${DEVICE}" ]; then
  echo "Select installation disk:"
  select opt in "${devicelist[@]}"
  do
    if ! [ -z "$opt" ]; then
      DEVICE=$(echo "$opt" | cut -d' ' -f1)
      break
    fi
  done
fi

# partition the selected disk
parted -s -a optimal -- ${DEVICE} \
  mklabel gpt \
  mkpart work btrfs 4MiB -4MiB

sleep 5

part_work="$(ls ${DEVICE}* | grep -E "^${DEVICE}p?1$")"

# force unmount partitions when running the script a second time
fuser -km ${part_work} || true

# create file systems and configuration space for grub
dd if=/dev/zero of=${part_work} bs=1M count=16 iflag=fullblock status=progress
mkfs.btrfs -L WORK ${part_work}

mkdir -m700 -p /install
mount --mkdir ${part_work} /install
btrfs subvol create /install/@
umount /install
mount -o subvol=@,compress=zstd:4,noatime ${part_work} /install

# enabling ufw filters inside container
modprobe iptable_filter
modprobe ip6table_filter

export TMPDIR="/install/tmp"
mkdir -p /install/run/containers/storage /install/var/lib/containers/storage "${TMPDIR}"
sed -i 's|/run/containers/storage|/install/run/containers/storage|g' /etc/containers/storage.conf
sed -i 's|/var/lib/containers/storage|/install/var/lib/containers/storage|g' /etc/containers/storage.conf
podman system migrate
echo "[#] podman info" && podman info
echo "[#] buildah info" && buildah info

buildah --cap-add=SYS_CHROOT,NET_ADMIN,NET_RAW --name worker from scratch
buildah config --entrypoint "/usr/sbin/init" --cmd '["--log-level=info", "--unit=multi-user.target"]' worker
scratchmnt=$(buildah mount worker)
mount --bind "${scratchmnt}" /mnt

pacstrap -K -c /mnt base

systemd-nspawn --capability=CAP_SYS_CHROOT,CAP_NET_ADMIN,CAP_NET_RAW --pipe -D /mnt <<EOX
# prepare systemd for container usage
# - disable login
ln -s /dev/null /etc/systemd/system/console-getty.service

# - disable first boot interactive config
ln -s /dev/null /etc/systemd/system/systemd-firstboot.service

# - disable time sync agent
ln -s /dev/null /etc/systemd/system/ntpd.service

# - logging to podman
mkdir -p /var/log/journal
sed -i 's/^#ForwardToConsole.*/ForwardToConsole=yes/' /etc/systemd/journald.conf

# - machine id setup for pam (systemd)
systemd-machine-id-setup

# - allow root login from /dev/pts/*
tee -a /etc/securetty <<EOS
pts/0
pts/1
pts/2
pts/3
pts/4
pts/5
pts/6
pts/7
pts/8
pts/9
EOS

# - protect /mnt from being writable to root user (used only as a mount point, protection against filling up your '/' on mount errors)
chmod a-rwx /mnt
chattr +i /mnt
EOX
