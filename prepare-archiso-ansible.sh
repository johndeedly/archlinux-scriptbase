#!/usr/bin/env bash

# initialize root
USER="$(id -un)"
if [ ! "$EUID" -eq 0 ]; then
  sudo "$0" "$@"
  exit $?
else
  echo "Running as user '$USER'"
fi

# parse parameters
MACHINEPATH="/mnt"
YES=""
POSITIONAL=()
while [ $# -gt 0 ]; do
  key="$1"
  case $key in
    -h|--help)
      shift
      echo "TODO"
      exit 1
      ;;
    -m|--machinepath)
      MACHINEPATH="$2"
      shift
      shift
      echo "[#] using MACHINEPATH=${MACHINEPATH}"
      ;;
    -y|--yes)
      YES="YES"
      shift
      echo "[#] assume yes on every option"
      ;;
    *)
      POSITIONAL+=("$1") # save it in an array for later
      shift
      ;;
  esac
done
set -- "${POSITIONAL[@]}" # restore positional parameters

# install ansible and prepare environment
rm /usr/lib/python3*/EXTERNALLY-MANAGED
python -m ensurepip
python -m pip install --upgrade pip
python -m pip install ansible-core
tee ~/.ansible.cfg <<EOF
[defaults]
interpreter_python=auto_silent
EOF

mkdir -p /var/lib/machines /etc/systemd/nspawn
mount --mkdir --bind "${MACHINEPATH}" /var/lib/machines/install
tee /etc/systemd/nspawn/install.nspawn <<EOF
[Network]
Port=tcp:61022:22
EOF
machinectl start install
