#!/usr/bin/env bash

# configure error handling
set -e -o pipefail

# read administrative database to get usernames, group names and home directories
ROOTID=$(getent passwd 0 | cut -d: -f1)
ROOTGRP=$(getent group "$(getent passwd 0 | cut -d: -f4)" | cut -d: -f1)
ROOTHOME=$(getent passwd 0 | cut -d: -f6)
USERID=$(getent passwd 1000 | cut -d: -f1)
USERGRP=$(getent group "$(getent passwd 1000 | cut -d: -f4)" | cut -d: -f1)
USERHOME=$(getent passwd 1000 | cut -d: -f6)
TEMPID=$(getent passwd 4815 | cut -d: -f1)
TEMPGRP=$(getent group "$(getent passwd 4815 | cut -d: -f4)" | cut -d: -f1)
TEMPHOME=$(getent passwd 4815 | cut -d: -f6)
echo "admin: ${ROOTID}|${ROOTGRP}|${ROOTHOME}"
echo "user: ${USERID}|${USERGRP}|${USERHOME}"
echo "temp: ${TEMPID}|${TEMPGRP}|${TEMPHOME}"

# install opensmtpd package
pacman -S --noconfirm --color=auto opensmtpd

# copy config files
cp /root/mailrelay/allows /etc/smtpd/allows
cp /root/mailrelay/smtpd.conf /etc/smtpd/smtpd.conf
cp /root/mailrelay/filter-rewrite-received /etc/smtpd/filter-rewrite-received
chmod +x /etc/smtpd/filter-rewrite-received

# generate password file
tee /etc/smtpd/secrets <<EOF
mx.domain.tld    $( smtpctl encrypt 'mx.domain.tld' )
EOF

# generate self signed cert
mkdir -p /etc/smtpd/tls
openssl req -nodes -newkey rsa:4096 -x509 -subj "/CN=mx.domain.tld/emailAddress=/C=/ST=/L=/O=/OU=" -keyout /etc/smtpd/tls/smtpd.key -out /etc/smtpd/tls/smtpd.crt

# enable service
systemctl enable smtpd.service

# == cleanup everything ==
# - tmpusr installation data
if [ -d "${TEMPHOME}" ]; then
  /usr/bin/rm -r "${TEMPHOME}"/* || true
fi
# - package cache
echo -en "y\ny\n" | ( LC_ALL=C pacman -Scc )
# - logs
if [ -f /run/systemd/journal/io.systemd.journal ]; then
  journalctl --rotate
  journalctl --vacuum-time=1s
fi
fstrim --all
# - do not rebuild cache on boot
tee /etc/.updated /var/.updated <<EOF
# For details, see manpage of systemd.unit -> 'ConditionNeedsUpdate'.
TIMESTAMP_NSEC=$(date +%s%N)
EOF
chmod 644 /etc/.updated /var/.updated
